package com.artemjev.backend.example.repository.vacation;

import com.artemjev.backend.example.domain.Vacation;
import com.artemjev.backend.example.dto.vacation.VacationFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


public class VacationRepositoryCustomImpl implements VacationRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Vacation> findByFilter(VacationFilter filter) {
        StringBuilder sb = new StringBuilder();
        sb.append("select v from Vacation v where 1=1");

        if (filter.getMasterId() != null) {
            sb.append(" and v.master.id = :masterId");
        }
        if (filter.getStartAt() != null) {
            sb.append(" and v.startAt >= (:startAtFrom)");
        }
        if (filter.getEndAt() != null) {
            sb.append(" and v.startAt < (:startAtTo)");
        }

        TypedQuery<Vacation> query = entityManager.createQuery(sb.toString(), Vacation.class);

        if (filter.getMasterId() != null) {
            query.setParameter("masterId", filter.getMasterId());
        }
        if (filter.getStartAt() != null) {
            query.setParameter("startAt", filter.getStartAt());
        }
        if (filter.getEndAt() != null) {
            query.setParameter("endAt", filter.getEndAt());
        }
        return query.getResultList();
    }
}
