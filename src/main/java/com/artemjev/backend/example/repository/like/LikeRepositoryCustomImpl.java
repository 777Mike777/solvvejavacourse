package com.artemjev.backend.example.repository.like;

import com.artemjev.backend.example.domain.Like;
import com.artemjev.backend.example.dto.like.LikeFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


public class LikeRepositoryCustomImpl implements LikeRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Like> findByFilter(LikeFilter filter) {
        StringBuilder sb = new StringBuilder();
        sb.append("select l from like l where 1=1");

        if (filter.getLike() != null) {
            sb.append(" and l.like = :like");
        }
        if (filter.getUserId() != null) {
            sb.append(" and l.user.id = :userId");
        }
        if (filter.getLikedObjectType() != null) {
            sb.append(" and l.liked.object.type = :likedObjectType");
        }
        if (filter.getLikedObjectId() != null) {
            sb.append(" and l.liked.object.id = :likedObjectId");
        }
        if (filter.getIsActive() != null) {
            sb.append(" and l.is.active = :isActive");
        }
        if (filter.getCreatedAtFrom() != null) {
            sb.append(" and l.createAt >= :createAtFrom");
        }
        if (filter.getCreatedAtTo() != null) {
            sb.append(" and l.createAt < (:createAtTo)");
        }
        if (filter.getUpdatedAtFrom() != null) {
            sb.append(" and l.updateAt >= (:updateAtFrom)");
        }
        if (filter.getUpdatedAtTo() != null) {
            sb.append(" and l.updateAt < (:updateAtTo)");
        }

        TypedQuery<Like> query = entityManager.createQuery(sb.toString(), Like.class);

        if (filter.getLike() != null) {
            query.setParameter("like", filter.getLike());
        }
        if (filter.getUserId() != null) {
            query.setParameter("userId", filter.getUserId());
        }
        if (filter.getLikedObjectType() != null) {
            query.setParameter("likedObjectType", filter.getLikedObjectType());
        }
        if (filter.getLikedObjectId() != null) {
            query.setParameter("likedObjectId", filter.getLikedObjectId());
        }
        if (filter.getIsActive() != null) {
            query.setParameter("isActive", filter.getIsActive());
        }
        if (filter.getCreatedAtFrom() != null) {
            query.setParameter("createAtFrom", filter.getCreatedAtFrom());
        }
        if (filter.getCreatedAtTo() != null) {
            query.setParameter("createAtTo", filter.getCreatedAtTo());
        }
        if (filter.getUpdatedAtFrom() != null) {
            query.setParameter("updateAtFrom", filter.getUpdatedAtFrom());
        }
        if (filter.getUpdatedAtTo() != null) {
            query.setParameter("", filter.getUpdatedAtTo());
        }

        return query.getResultList();
    }
}
