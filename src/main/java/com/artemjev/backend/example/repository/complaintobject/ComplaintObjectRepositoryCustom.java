package com.artemjev.backend.example.repository.complaintobject;

import com.artemjev.backend.example.domain.ComplaintObject;
import com.artemjev.backend.example.dto.complaintobject.ComplaintObjectFilter;

import java.util.List;
import java.util.UUID;


public interface ComplaintObjectRepositoryCustom {
    List<ComplaintObject> findByFilter(UUID complaintId, ComplaintObjectFilter filter);
}
