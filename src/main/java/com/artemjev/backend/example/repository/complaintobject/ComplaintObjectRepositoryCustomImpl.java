package com.artemjev.backend.example.repository.complaintobject;

import com.artemjev.backend.example.domain.ComplaintObject;
import com.artemjev.backend.example.dto.complaintobject.ComplaintObjectFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;


public class ComplaintObjectRepositoryCustomImpl implements ComplaintObjectRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ComplaintObject> findByFilter(UUID complaintId, ComplaintObjectFilter complaintObjectFilter) {
        StringBuilder sb = new StringBuilder();

        sb.append("select co from complaint_object co where "
                + "co.complaint.id = :complaintId");
        if (complaintObjectFilter.getObjectType() != null) {
            sb.append(" and co.objectType = :objectType");
        }
        if (complaintObjectFilter.getObjectId() != null) {
            sb.append(" and co.objectId = :objectId");
        }
        if (complaintObjectFilter.getObjectFieldName() != null) {
            sb.append(" and co.objectFieldName = :objectFieldName");
        }
        if (complaintObjectFilter.getNote() != null) {
            sb.append(" and co.note = :note");
        }
        if (complaintObjectFilter.getCreatedAtFrom() != null) {
            sb.append(" and co.createAt >= (:createAtFrom)");
        }
        if (complaintObjectFilter.getCreatedAtTo() != null) {
            sb.append(" and co.createAt < (:createAtTo)");
        }
        if (complaintObjectFilter.getUpdatedAtFrom() != null) {
            sb.append(" and co.updateAt >= (:updateAtFrom)");
        }
        if (complaintObjectFilter.getUpdatedAtTo() != null) {
            sb.append(" and co.updateAt < (:updateAtTo)");
        }

        TypedQuery<ComplaintObject> query = entityManager.createQuery(sb.toString(), ComplaintObject.class);

        query.setParameter("complaintId", complaintId);
        if (complaintObjectFilter.getObjectType() != null) {
            query.setParameter("objectType", complaintObjectFilter.getObjectType());
        }
        if (complaintObjectFilter.getObjectId() != null) {
            query.setParameter("objectId", complaintObjectFilter.getObjectId());
        }
        if (complaintObjectFilter.getObjectFieldName() != null) {
            query.setParameter("objectFieldName", complaintObjectFilter.getObjectFieldName());
        }
        if (complaintObjectFilter.getNote() != null) {
            query.setParameter("note", complaintObjectFilter.getNote());
        }
        if (complaintObjectFilter.getCreatedAtFrom() != null) {
            query.setParameter("createAtFrom", complaintObjectFilter.getCreatedAtFrom());
        }
        if (complaintObjectFilter.getCreatedAtTo() != null) {
            query.setParameter("createAtTo", complaintObjectFilter.getCreatedAtTo());
        }
        if (complaintObjectFilter.getUpdatedAtFrom() != null) {
            query.setParameter("updateAtFrom", complaintObjectFilter.getUpdatedAtFrom());
        }
        if (complaintObjectFilter.getUpdatedAtTo() != null) {
            query.setParameter("", complaintObjectFilter.getUpdatedAtTo());
        }
        return query.getResultList();
    }
}
