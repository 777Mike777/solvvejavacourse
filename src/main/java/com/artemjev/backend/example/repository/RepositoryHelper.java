package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Optional;
import java.util.UUID;


@Component
public class RepositoryHelper {

    @PersistenceContext
    private EntityManager entityManager;

    //---------------------------------------------------------------------
    public <E> E getEntityRequired(Class<E> entityClass, UUID id) {

        return Optional.ofNullable(entityManager.find(entityClass, id)).orElseThrow(() ->
                new EntityNotFoundException(entityClass, id));
    }

    //---------------------------------------------------------------------
//    public <E> E getRelatedEntityRequired(Class<E> targetClass, UUID targetId,
//                                          Class<E> sourceClass, UUID sourceId) {
//
//        validateExists(targetClass, targetId);
//
//        E sourceEntity = getEntityRequired(sourceClass, sourceId);
//
//       /* if (!complaintObject.getComplaint().getId().equals(complaintId)) {
//            //TODO: создать отдельное исключение для случая, когда при запросе связанной сущности id хозяина в базе
//            // не совпадет с тем, что указан в параметрах запроса... и бросать его!
//            throw new IllegalArgumentException(String.format("Entity %s with id=%s contains a foreign key "
//                    + "to entity %s that differs from %s", "ComplaintObject", id, "Complaint", complaintId));
//        }*/
//
//        return Optional.ofNullable(entityManager.find(sourceClass, sourceId)).orElseThrow(() ->
//                new EntityNotFoundException(sourceClass, sourceId));
//    }


    //---------------------------------------------------------------------
    public <E> E getReferenceIfExist(Class<E> entityClass, UUID id) {
        validateExists(entityClass, id);
        return entityManager.getReference(entityClass, id);
    }

    public <E> void validateExists(Class<E> entityClass, UUID id) {
        Query query = entityManager.createQuery("select count (e) from  " + entityClass.getSimpleName()
                + " e where e.id = :id");
        query.setParameter("id", id);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityClass, id);
        }
    }

}
