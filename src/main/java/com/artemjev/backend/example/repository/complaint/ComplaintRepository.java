package com.artemjev.backend.example.repository.complaint;

import com.artemjev.backend.example.domain.Complaint;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface ComplaintRepository extends CrudRepository<Complaint, UUID> {
}
