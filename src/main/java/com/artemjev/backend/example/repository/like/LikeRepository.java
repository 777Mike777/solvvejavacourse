package com.artemjev.backend.example.repository.like;

import com.artemjev.backend.example.domain.Like;
import com.artemjev.backend.example.dto.like.LikeFilter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository
public interface LikeRepository extends CrudRepository<Like, UUID>, LikeRepositoryCustom {
    List<Like> findByFilter(LikeFilter filter);
}
