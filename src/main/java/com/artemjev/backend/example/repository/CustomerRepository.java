package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface CustomerRepository extends CrudRepository<Customer, UUID> {
}
