package com.artemjev.backend.example.repository.visit;

import com.artemjev.backend.example.domain.Visit;
import com.artemjev.backend.example.dto.visit.VisitFilter;

import java.util.List;


public interface VisitRepositoryCustom {
    List<Visit> findByFilter(VisitFilter filter);
}
