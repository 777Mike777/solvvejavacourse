package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.domain.RegUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface RegUserRepository extends CrudRepository<RegUser, UUID> {
}
