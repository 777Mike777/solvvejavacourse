package com.artemjev.backend.example.repository.vacation;

import com.artemjev.backend.example.domain.Vacation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface VacationRepository extends CrudRepository<Vacation, UUID>, VacationRepositoryCustom {
}
