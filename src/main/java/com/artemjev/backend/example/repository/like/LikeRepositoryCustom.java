package com.artemjev.backend.example.repository.like;

import com.artemjev.backend.example.domain.Like;
import com.artemjev.backend.example.dto.like.LikeFilter;

import java.util.List;


public interface LikeRepositoryCustom {
    List<Like> findByFilter(LikeFilter filter);
}
