package com.artemjev.backend.example.repository.misprint;

import com.artemjev.backend.example.domain.Misprint;
import com.artemjev.backend.example.dto.misprint.MisprintFilter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.UUID;


@Repository
public interface MisprintRepository extends CrudRepository<Misprint, UUID>, MisprintRepositoryCustom {

//    @Query("select v from Misprint v where v.master.id = :masterId and v.status = :misprintStatus"
//            + " and v.startAt >= :startFrom and v.startAt < :startTo order by v.startAt asc")
//    List<Misprint> findMisprintsForMasterInGivenInterval(UUID masterId, MisprintStatus misprintStatus,
//                                                         Instant startFrom, Instant startTo);
    List<Misprint> findByFilter(MisprintFilter filter);

}
