package com.artemjev.backend.example.repository.vacation;

import com.artemjev.backend.example.domain.Vacation;
import com.artemjev.backend.example.dto.vacation.VacationFilter;

import java.util.List;


public interface VacationRepositoryCustom {
    List<Vacation> findByFilter(VacationFilter filter);
}
