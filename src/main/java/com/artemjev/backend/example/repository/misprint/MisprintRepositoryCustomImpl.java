package com.artemjev.backend.example.repository.misprint;

import com.artemjev.backend.example.domain.Misprint;
import com.artemjev.backend.example.dto.misprint.MisprintFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


public class MisprintRepositoryCustomImpl implements MisprintRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Misprint> findByFilter(MisprintFilter filter) {

        StringBuilder sb = new StringBuilder();
        sb.append("select m from Misprint m where 1=1");

        if (filter.getUserSuppliantId() != null) {
            sb.append(" and m.UserSuppliantId = :userSuppliantId");
        }
        if (filter.getStatuses() != null && !filter.getStatuses().isEmpty()) {
            sb.append(" and m.status in (:statuses)");
        }
        if (filter.getMisprintObjectId() != null) {
            sb.append(" and m.misprintObjectId = :misprintObjectId");
        }

        if (filter.getMisprintObjectFieldName() != null) {
            sb.append(" and m.misprintObjectFieldName = :misprintObjectFieldName");
        }
        if (filter.getActualFieldValue() != null) {
            sb.append(" and m.actualFieldValue = :actualFieldValue");
        }
        if (filter.getSuggestedFieldValue() != null) {
            sb.append(" and m.suggestedFieldValue = :suggestedFieldValue");
        }
        if (filter.getCorrectedByContentManagerId() != null) {
            sb.append(" and m.correctedByContentManagerId = :correctedByContentManagerId");
        }
        if (filter.getCorrectedAtFrom() != null) {
            sb.append(" and m.correctedAt >= (:CorrectedAtFrom)");
        }
        if (filter.getCorrectedAtTo() != null) {
            sb.append(" and m.correctedAt < (:CorrectedAtTo)");
        }

        TypedQuery<Misprint> query = entityManager.createQuery(sb.toString(), Misprint.class);

        if (filter.getUserSuppliantId() != null) {
            query.setParameter("userSuppliantId", filter.getUserSuppliantId());
        }
        if (filter.getStatuses() != null && !filter.getStatuses().isEmpty()) {
            query.setParameter("statuses", filter.getStatuses());
        }
        if (filter.getMisprintObjectId() != null) {
            query.setParameter("misprintObjectId", filter.getMisprintObjectId());
        }
        if (filter.getMisprintObjectFieldName() != null) {
            query.setParameter("misprintObjectFieldName", filter.getMisprintObjectFieldName());
        }
        if (filter.getActualFieldValue() != null) {
            query.setParameter("actualFieldValue", filter.getActualFieldValue());
        }
        if (filter.getSuggestedFieldValue() != null) {
            query.setParameter("suggestedFieldValue", filter.getSuggestedFieldValue());
        }
        if (filter.getCorrectedByContentManagerId() != null) {
            query.setParameter("correctedByContentManagerId", filter.getCorrectedByContentManagerId());
        }
        if (filter.getCorrectedAtFrom() != null) {
            query.setParameter("correctedAtFrom", filter.getCorrectedAtFrom());
        }
        if (filter.getCorrectedAtTo() != null) {
            query.setParameter("correctedAtTo", filter.getCorrectedAtTo());
        }

        return query.getResultList();
    }
}