package com.artemjev.backend.example.repository.visit;

import com.artemjev.backend.example.domain.Visit;
import com.artemjev.backend.example.domain.VisitStatus;
import com.artemjev.backend.example.dto.visit.VisitFilter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public interface VisitRepository extends CrudRepository<Visit, UUID>, VisitRepositoryCustom {

    List<Visit> findByMasterIdAndStatusOrderByStartAtAsc(UUID masterId, VisitStatus visitStatus);

    List<Visit> findByMasterIdAndStatusAndStartAtGreaterThanEqualAndStartAtLessThanOrderByStartAtAsc(UUID masterId,
                                                                                                     VisitStatus visSt,
                                                                                                     Instant startFrom,
                                                                                                     Instant startTo);

    @Query("select v from Visit v where v.master.id = :masterId and v.status = :visitStatus"
            + " and v.startAt >= :startFrom and v.startAt < :startTo order by v.startAt asc")
    List<Visit> findVisitsForMasterInGivenInterval(UUID masterId, VisitStatus visitStatus,
                                                   Instant startFrom, Instant startTo);

    List<Visit> findByFilter(VisitFilter filter);

    @Query("select avg(v.customerMark) from Visit v where v.master.id = :masterId")
    Double calcAverageMarkOfMaster(UUID masterId);
}
