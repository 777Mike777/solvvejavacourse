package com.artemjev.backend.example.repository.misprint;

import com.artemjev.backend.example.domain.Misprint;
import com.artemjev.backend.example.dto.misprint.MisprintFilter;

import java.util.List;


public interface MisprintRepositoryCustom {

    List<Misprint> findByFilter(MisprintFilter filter);
}
