package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.domain.Master;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.stream.Stream;


@Repository
public interface MasterRepository extends CrudRepository<Master, UUID> {

    @Query("select m.id from Master m")
    Stream<UUID> getIdsOfMasters();
}
