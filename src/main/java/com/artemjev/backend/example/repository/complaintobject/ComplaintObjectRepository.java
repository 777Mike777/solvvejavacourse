package com.artemjev.backend.example.repository.complaintobject;

import com.artemjev.backend.example.domain.ComplaintObject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface ComplaintObjectRepository extends CrudRepository<ComplaintObject, UUID>,
        ComplaintObjectRepositoryCustom {
}
