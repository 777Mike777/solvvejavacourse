package com.artemjev.backend.example.job;

import com.artemjev.backend.example.repository.MasterRepository;
import com.artemjev.backend.example.service.MasterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Component
public class UpdateAverageMarkOfMasterJob {

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private MasterService masterService;

    @Transactional(readOnly = true)
    public void updateAverageMarkOfMasters() {
        log.info("Job started");

        masterRepository.getIdsOfMasters().forEach(masterId -> {
            try {
                masterService.updateAverageMarkOfMaster(masterId);
            } catch (Exception e) {
                log.error("Failed to update average mark for master: {}", masterId, e);
            }
        });

        log.info("Job finished");
    }
}

