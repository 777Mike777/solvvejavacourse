package com.artemjev.backend.example.domain;

import lombok.Getter;
import lombok.Setter;
import org.dom4j.tree.AbstractEntity;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Complaint extends AbstractEntity {

    @Id
    @GeneratedValue
    private UUID id;

    @Enumerated(EnumType.STRING)
    private ComplaintStatus status;

    @ManyToOne
    private RegUser user;

    @Enumerated(EnumType.STRING)
    private ComplaintTargType targetType;

    @OneToMany(mappedBy = "complaint")
    private List<ComplaintObject> complaintObjects = new ArrayList<>();

    private String complaintMessage;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;


}
