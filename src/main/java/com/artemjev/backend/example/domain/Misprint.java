package com.artemjev.backend.example.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;


@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Misprint {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    private RegUser userSuppliant;

    @Enumerated(EnumType.STRING)
    private MisprintStatus status;

    @ManyToOne
    private DomainObject misprintObject;

    private String misprintObjectFieldName;

    private String actualFieldValue;

    private String suggestedFieldValue;

    @ManyToOne
    private ContentManager correctedByContentManager;

    private Instant correctedAt;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
