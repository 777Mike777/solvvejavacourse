package com.artemjev.backend.example.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@Entity
public class Film {

    @Id
    @GeneratedValue
    private UUID id;

    private String title;

    private LocalDate primeDate;

    @Enumerated(EnumType.STRING)
    private FilmStatus status;

    private String info;

    private String plot;

    private Double averageMark;
    private Double forecastAverageMark;

    @ManyToMany
    @JoinTable(name = "film_genre",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> genres = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "film_source_language",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "language_id"))
    private List<Language> sourceLanguages = new ArrayList<>();


    //@OneToMany(mappedBy = "film")
    //private List<DubbedLanguage> dubbedLanguages;

    @OneToMany(mappedBy = "film")
    private List<Role> roles;


    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
