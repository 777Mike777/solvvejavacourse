package com.artemjev.backend.example.domain;

public enum MisprintStatus {
    FIXED,
    NEED_TO_FIX,
    CANCELED
}