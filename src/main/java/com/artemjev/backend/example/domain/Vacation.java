package com.artemjev.backend.example.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;


@Getter
@Setter
@Entity
public class Vacation {

    @Id
    @GeneratedValue
    private UUID id;

    private LocalDate startAt;
    private LocalDate endAt;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Master master;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
