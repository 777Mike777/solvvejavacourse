package com.artemjev.backend.example.domain;

public enum NoveltyStatus {
    DRAFT,
    POSTED,
    ARCHIVE
}