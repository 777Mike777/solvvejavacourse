package com.artemjev.backend.example.domain;

public enum DomainObjectType {
    REVIEW,
    FILM,
    ROLE,
    ACTOR,
    PERSON,
    NOVELTY
}
