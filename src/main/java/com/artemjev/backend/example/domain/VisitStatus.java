package com.artemjev.backend.example.domain;

public enum VisitStatus {
    SCHEDULED,
    FINISHED,
    CANCELLED
}
