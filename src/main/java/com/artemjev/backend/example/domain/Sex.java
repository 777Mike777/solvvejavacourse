package com.artemjev.backend.example.domain;

public enum Sex {
    MALE,
    FEMALE,
    UNDEFINED,
    GAY,
    LESBIAN,
    BISEXUAL
}
