package com.artemjev.backend.example.domain;

public enum ComplaintTargType {
    MODERATOR,
    CONTENT_MANAGER
}
