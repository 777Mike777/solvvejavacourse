package com.artemjev.backend.example.domain;

import lombok.Getter;
import lombok.Setter;
import org.dom4j.tree.AbstractEntity;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;


@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Role extends AbstractEntity {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    private Integer age;

    @ManyToOne
    private Actor actor;

    @ManyToOne
    private Film film;

    private String sex;

    @Enumerated(EnumType.STRING)
    private RoleType type;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

}
