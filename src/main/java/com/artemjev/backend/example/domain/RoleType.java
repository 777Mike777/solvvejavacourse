package com.artemjev.backend.example.domain;

public enum RoleType {
    MAIN_ROLE,
    SUPPORTING_ROLE,
    STUNTMAN
}
