package com.artemjev.backend.example.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Genre {

    @Id
    @GeneratedValue
    private UUID id;

    String title;

    @ManyToMany(mappedBy = "sourceLanguages")
    private List<Film> genres = new ArrayList<>();

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
