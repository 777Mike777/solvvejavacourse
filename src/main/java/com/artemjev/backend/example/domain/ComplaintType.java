package com.artemjev.backend.example.domain;

public enum ComplaintType {
    UNMARKED_SPOILER,
    ADVERTISING,
    INSULT,
    THREAT,
    DISCRIMINATION
}
