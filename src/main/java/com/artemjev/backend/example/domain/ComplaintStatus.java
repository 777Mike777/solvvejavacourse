package com.artemjev.backend.example.domain;

public enum ComplaintStatus {
    UNTRIED,
    IN_PROCESSING,
    CORRECTED,
    CANCELLED
}
