package com.artemjev.backend.example.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;


@Getter
@Setter
@Entity
public class Novelty {

    @Id
    @GeneratedValue
    private UUID id;

    private String title;

    private Instant time;

    private String content;

    @Enumerated(EnumType.STRING)
    private NoveltyStatus status;

    private Integer likeNumber;
    private Integer dislikeNumber;
    private Integer viewsNumber;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
