package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Misprint;
import com.artemjev.backend.example.dto.misprint.MisprintCreateDTO;
import com.artemjev.backend.example.dto.misprint.MisprintFilter;
import com.artemjev.backend.example.dto.misprint.MisprintProcessDTO;
import com.artemjev.backend.example.dto.misprint.MisprintReadDTO;
import com.artemjev.backend.example.repository.RepositoryHelper;
import com.artemjev.backend.example.repository.misprint.MisprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class MisprintService {

    @Autowired
    private MisprintRepository misprintRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MisprintReadDTO getMisprint(UUID id) {
        Misprint Misprint = repositoryHelper.getEntityRequired(Misprint.class, id);
        return translationService.toRead(Misprint);
    }

    public List<MisprintReadDTO> getMisprints(MisprintFilter filter) {
        List<Misprint> misprints = misprintRepository.findByFilter(filter);
        return misprints.stream().map(translationService::toRead).collect(Collectors.toList());
    }

    public MisprintReadDTO createMisprint(MisprintCreateDTO create) {
        Misprint misprint = translationService.toEntity(create);
        misprint = misprintRepository.save(misprint);
        return translationService.toRead(misprint);
    }
    public MisprintReadDTO processMisprint(MisprintProcessDTO process) {
        Misprint misprint = translationService.toEntity(process);
        misprint = misprintRepository.save(misprint);
        return translationService.toRead(misprint);
    }

}
