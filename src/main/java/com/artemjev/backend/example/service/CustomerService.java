package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Customer;
import com.artemjev.backend.example.dto.customer.CustomerCreateDTO;
import com.artemjev.backend.example.dto.customer.CustomerPatchDTO;
import com.artemjev.backend.example.dto.customer.CustomerPutDTO;
import com.artemjev.backend.example.dto.customer.CustomerReadDTO;
import com.artemjev.backend.example.repository.CustomerRepository;
import com.artemjev.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;


    public CustomerReadDTO getCustomer(UUID id) {
        Customer customer = repositoryHelper.getEntityRequired(Customer.class, id);
        return translationService.toRead(customer);
    }

    public CustomerReadDTO createCustomer(CustomerCreateDTO create) {
        Customer customer = translationService.toEntity(create);

        customer = customerRepository.save(customer);
        return translationService.toRead(customer);
    }

    public CustomerReadDTO patchCustomer(UUID id, CustomerPatchDTO patch) {
        Customer customer = repositoryHelper.getEntityRequired(Customer.class, id);
        translationService.patchEntity(patch, customer);
        customer = customerRepository.save(customer);
        return translationService.toRead(customer);
    }

    public CustomerReadDTO updateCustomer(UUID id, CustomerPutDTO put) {
        Customer customer = repositoryHelper.getEntityRequired(Customer.class, id);
        translationService.patchEntity(put, customer);

        customer = customerRepository.save(customer);
        return translationService.toRead(customer);
    }

    public void deleteCustomer(UUID id) {
        customerRepository.delete(repositoryHelper.getEntityRequired(Customer.class, id));
    }

}
