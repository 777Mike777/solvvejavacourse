package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Complaint;
import com.artemjev.backend.example.domain.ComplaintObject;
import com.artemjev.backend.example.dto.complaintobject.*;
import com.artemjev.backend.example.repository.RepositoryHelper;
import com.artemjev.backend.example.repository.complaintobject.ComplaintObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class ComplaintObjectService {

    @Autowired
    private ComplaintObjectRepository complaintObjectRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ComplaintObjectReadDTO getComplaintObject(UUID complaintId, UUID id) {
        ComplaintObject complaintObject = repositoryHelper.getEntityRequired(ComplaintObject.class, id);
        return translationService.toRead(complaintObject);
    }

    public List<ComplaintObjectReadDTO> getComplaintObjects(UUID complaintId, ComplaintObjectFilter filter) {
        repositoryHelper.validateExists(Complaint.class, complaintId);

        List<ComplaintObject> complaintObjects = complaintObjectRepository.findByFilter(complaintId, filter);
        return complaintObjects.stream().map(translationService::toRead).collect(Collectors.toList());
    }

    public ComplaintObjectReadDTO createComplaintObject(UUID complaintId, ComplaintObjectCreateDTO create) {
        ComplaintObject complaintObject = translationService.toEntity(create);
        complaintObject.setComplaint(repositoryHelper.getReferenceIfExist(Complaint.class, complaintId));

        complaintObject = complaintObjectRepository.save(complaintObject);
        return translationService.toRead(complaintObject);
    }

    public ComplaintObjectReadDTO patchComplaintObject(UUID complaintId, UUID id, ComplaintObjectPatchDTO patch) {
        ComplaintObject complaintObject = repositoryHelper.getEntityRequired(ComplaintObject.class, id);
        translationService.patchEntity(patch, complaintObject);

        complaintObject = complaintObjectRepository.save(complaintObject);
        return translationService.toRead(complaintObject);
    }

    public ComplaintObjectReadDTO updateComplaintObject(UUID complaintId, UUID id, ComplaintObjectPutDTO put) {
        ComplaintObject complaintObject = repositoryHelper.getEntityRequired(ComplaintObject.class, id);
        translationService.patchEntity(put, complaintObject);

        complaintObject = complaintObjectRepository.save(complaintObject);
        return translationService.toRead(complaintObject);
    }

    public void deleteComplaintObject(UUID complaintId, UUID id) {
        complaintObjectRepository.delete(repositoryHelper.getEntityRequired(ComplaintObject.class, id));
    }

}
