package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.*;
import com.artemjev.backend.example.dto.MasterReadDTO;
import com.artemjev.backend.example.dto.complaintobject.ComplaintObjectCreateDTO;
import com.artemjev.backend.example.dto.complaintobject.ComplaintObjectPatchDTO;
import com.artemjev.backend.example.dto.complaintobject.ComplaintObjectPutDTO;
import com.artemjev.backend.example.dto.complaintobject.ComplaintObjectReadDTO;
import com.artemjev.backend.example.dto.customer.CustomerCreateDTO;
import com.artemjev.backend.example.dto.customer.CustomerPatchDTO;
import com.artemjev.backend.example.dto.customer.CustomerPutDTO;
import com.artemjev.backend.example.dto.customer.CustomerReadDTO;
import com.artemjev.backend.example.dto.like.LikeCreateDTO;
import com.artemjev.backend.example.dto.like.LikeReadDTO;
import com.artemjev.backend.example.dto.misprint.MisprintCreateDTO;
import com.artemjev.backend.example.dto.misprint.MisprintProcessDTO;
import com.artemjev.backend.example.dto.misprint.MisprintReadDTO;
import com.artemjev.backend.example.dto.person.PersonReadDTO;
import com.artemjev.backend.example.dto.vacation.VacationCreateDTO;
import com.artemjev.backend.example.dto.vacation.VacationReadDTO;
import com.artemjev.backend.example.dto.visit.VisitCreateDTO;
import com.artemjev.backend.example.dto.visit.VisitReadDTO;
import com.artemjev.backend.example.dto.visit.VisitReadExtendedDTO;
import com.artemjev.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    public PersonReadDTO toRead(Person person) {
        PersonReadDTO dto = new PersonReadDTO();
        dto.setId(person.getId());
        dto.setFirstName(person.getFirstName());
        dto.setLastName(person.getLastName());
        dto.setPhone(person.getPhone());
        dto.setSex(person.getSex());
        dto.setBirthDay(person.getBirthDay());
        dto.setDeathDay(person.getDeathDay());
        dto.setMaritalStatus(person.getMaritalStatus());
        dto.setChildrenAmount(person.getChildrenAmount());
        dto.setNationality(person.getNationality());
        dto.setCitizenship(person.getCitizenship());
        dto.setResidencePlace(person.getResidencePlace());
        dto.setAutobiography(person.getAutobiography());
        dto.setAdditionalInfo(person.getAdditionalInfo());
        dto.setCreatedAt(person.getCreatedAt());
        dto.setUpdatedAt(person.getUpdatedAt());
        return dto;
    }

    public MisprintReadDTO toRead(Misprint misprint) {
        MisprintReadDTO dto = new MisprintReadDTO();
        dto.setId(misprint.getId());
        dto.setUserSuppliantId(misprint.getUserSuppliant().getId());
        dto.setStatus(misprint.getStatus());
        dto.setMisprintObjectId(misprint.getMisprintObject().getId());
        dto.setMisprintObjectFieldName(misprint.getMisprintObjectFieldName());
        dto.setActualFieldValue(misprint.getActualFieldValue());
        dto.setSuggestedFieldValue(misprint.getSuggestedFieldValue());
        dto.setCorrectedByContentManagerId(misprint.getCorrectedByContentManager().getId());
        dto.setCorrectedAt(misprint.getCorrectedAt());
        dto.setCreatedAt(misprint.getCreatedAt());
        dto.setUpdatedAt(misprint.getUpdatedAt());
        return dto;
    }

    public LikeReadDTO toRead(Like like) {
        LikeReadDTO dto = new LikeReadDTO();
        dto.setId(like.getId());
        dto.setLike(like.getLike());
        dto.setUserId(like.getUser().getId());
        dto.setLikedObjectType(like.getLikedObjectType());
        dto.setLikedObjectId(like.getLikedObjectId());
        dto.setIsActive(like.getIsActive());
        dto.setCreatedAt(like.getCreatedAt());
        dto.setUpdatedAt(like.getUpdatedAt());
        return dto;
    }

    public ComplaintObjectReadDTO toRead(ComplaintObject complaintObject) {
        ComplaintObjectReadDTO dto = new ComplaintObjectReadDTO();
        dto.setId(complaintObject.getId());
        dto.setComplaintId(complaintObject.getComplaint().getId());
        dto.setObjectType(complaintObject.getObjectType());
        dto.setObjectId(complaintObject.getObjectId());
        dto.setObjectFieldName(complaintObject.getObjectFieldName());
        dto.setNote(complaintObject.getNote());
        dto.setCreatedAt(complaintObject.getCreatedAt());
        dto.setUpdatedAt(complaintObject.getUpdatedAt());
        return dto;
    }

    public CustomerReadDTO toRead(Customer customer) {
        CustomerReadDTO dto = new CustomerReadDTO();
        dto.setId(customer.getId());
        dto.setName(customer.getName());
        dto.setPhone(customer.getPhone());
        dto.setCreatedAt(customer.getCreatedAt());
        dto.setUpdatedAt(customer.getUpdatedAt());
        return dto;
    }

    public MasterReadDTO toRead(Master master) {
        MasterReadDTO dto = new MasterReadDTO();
        dto.setId(master.getId());
        dto.setName(master.getName());
        dto.setPhone(master.getPhone());
        dto.setAbout(master.getAbout());
        dto.setCreatedAt(master.getCreatedAt());
        dto.setUpdatedAt(master.getUpdatedAt());
        return dto;
    }

    public VisitReadDTO toRead(Visit visit) {
        VisitReadDTO dto = new VisitReadDTO();
        dto.setId(visit.getId());
        dto.setStartAt(visit.getStartAt());
        dto.setFinishAt(visit.getFinishAt());
        dto.setStatus(visit.getStatus());
        dto.setCustomerId(visit.getCustomer().getId());
        dto.setMasterId(visit.getMaster().getId());
        dto.setCreatedAt(visit.getCreatedAt());
        dto.setUpdatedAt(visit.getUpdatedAt());
        return dto;
    }

    public VacationReadDTO toRead(Vacation vacation) {
        VacationReadDTO dto = new VacationReadDTO();
        dto.setId(vacation.getId());
        dto.setStartAt(vacation.getStartAt());
        dto.setEndAt(vacation.getEndAt());
        dto.setMasterId(vacation.getMaster().getId());
        dto.setCreatedAt(vacation.getCreatedAt());
        dto.setUpdatedAt(vacation.getUpdatedAt());
        return dto;
    }

    //------------------------------------------------------------

    public Misprint toEntity(MisprintProcessDTO process) {
        Misprint misprint = new Misprint();
        //TODO:
        return misprint;
    }


    public Misprint toEntity(MisprintCreateDTO create) {
        Misprint misprint = new Misprint();
        misprint.setMisprintObject(repositoryHelper.getReferenceIfExist(DomainObject.class,
                create.getMisprintObjectId()));
        misprint.setMisprintObjectFieldName(create.getMisprintObjectFieldName());
        misprint.setActualFieldValue(create.getActualFieldValue());
        misprint.setSuggestedFieldValue(create.getSuggestedFieldValue());
        misprint.setStatus(MisprintStatus.NEED_TO_FIX);
        return misprint;
    }

    public Like toEntity(LikeCreateDTO create) {
        Like like = new Like();
        like.setLike(create.getLike());
        like.setUser(repositoryHelper.getReferenceIfExist(RegUser.class, create.getUserId()));
        like.setLikedObjectType(create.getLikedObjectType());
        like.setLikedObjectId(create.getLikedObjectId());
        like.setIsActive(true);
        return like;
    }

    public ComplaintObject toEntity(ComplaintObjectCreateDTO create) {
        ComplaintObject complaintObject = new ComplaintObject();
        complaintObject.setObjectType(create.getObjectType());
        complaintObject.setObjectId(create.getObjectId());
        complaintObject.setObjectFieldName(create.getObjectFieldName());
        complaintObject.setNote(create.getNote());
        return complaintObject;
    }

    public Customer toEntity(CustomerCreateDTO create) {
        Customer customer = new Customer();
        customer.setName(create.getName());
        customer.setPhone(create.getPhone());
        return customer;
    }

    public Visit toEntity(VisitCreateDTO create) {
        Visit visit = new Visit();
        visit.setStartAt(create.getStartAt());
        visit.setFinishAt(create.getFinishAt());
        visit.setStatus(VisitStatus.SCHEDULED);
        visit.setMaster(repositoryHelper.getReferenceIfExist(Master.class, create.getMasterId()));
        visit.setCustomer(repositoryHelper.getReferenceIfExist(Customer.class, create.getCustomerId()));
        return visit;
    }

    public Vacation toEntity(VacationCreateDTO create) {
        Vacation vacation = new Vacation();
        vacation.setStartAt(create.getStartAt());
        vacation.setEndAt(create.getEndAt());
        return vacation;
    }

    //------------------------------------------------------------
    public void patchEntity(ComplaintObjectPatchDTO patch, ComplaintObject complaintObject) {

        if (patch.getComplaintId() != null) {
            complaintObject.setComplaint(repositoryHelper.getReferenceIfExist(Complaint.class,
                    patch.getComplaintId()));
        }

        //TODO: нужно еще проверять, что сущность (объект жалобы) с таким id существует в соответствующей таблице.
        if (patch.getObjectId() != null) {
            complaintObject.setObjectId(patch.getObjectId());
        }

        //TODO: поле objectType - это, по сути, название таблицы/сущности, к которой относиться объект жалобы.
        // Т.е. при изменении поля objectType и/или поля objectId, нужно проверять перед сохранением в базу
        // (в одной же транзакции, наверное), что сущность соответствующая нашим objectType и objectId существует!
        if (patch.getObjectType() != null) {
            complaintObject.setObjectType(patch.getObjectType());
        }

        if (patch.getObjectFieldName() != null) {
            complaintObject.setObjectFieldName(patch.getObjectFieldName());
        }

        if (patch.getNote() != null) {
            complaintObject.setNote(patch.getNote());
        }
    }

    public void patchEntity(CustomerPatchDTO patch, Customer customer) {
        if (patch.getPhone() != null) {
            customer.setPhone(patch.getPhone());
        }
        if (patch.getName() != null) {
            customer.setName(patch.getName());
        }
    }

    //------------------------------------------------------------
    public void patchEntity(ComplaintObjectPutDTO put, ComplaintObject complaintObject) {

        // Т.к. это композиция и объект жалобы не имеет смысла без самой жалобы,
        // делаем проверку, что put.getComplaintId() != null.
        if (put.getComplaintId() != null) {
            complaintObject.setComplaint(repositoryHelper.getReferenceIfExist(Complaint.class, put.getComplaintId()));
        }

        //По логике, сущность ComplaintObject лишена всякого смысла, если вней не определены поля
        // ObjectId и setObjectType, поэтому проверяем при апдейте, что эти поля != null
        //TODO: нужно еще проверять, что сущность (объект жалобы) с таким id существует в соответствующей таблице.
        if (put.getObjectId() != null) {
            complaintObject.setObjectId(put.getObjectId());
        }

        //TODO: поле objectType - это, по сути, название таблицы/сущности, к которой относиться объект жалобы.
        // Т.е. при изменении поля objectType и/или поля objectId, нужно проверять перед сохранением в базу
        // (в одной же транзакции, наверное), что сущность соответствующая нашим objectType и objectId существует!
        if (put.getObjectType() != null) {
            complaintObject.setObjectType(put.getObjectType());
        }

        complaintObject.setObjectFieldName(put.getObjectFieldName());
        complaintObject.setNote(put.getNote());
    }

    public void patchEntity(CustomerPutDTO put, Customer customer) {
        customer.setPhone(put.getPhone());
        customer.setName(put.getName());
    }

    //------------------------------------------------------------
    public VisitReadExtendedDTO toReadExtended(Visit visit) {
        VisitReadExtendedDTO dto = new VisitReadExtendedDTO();
        dto.setId(visit.getId());
        dto.setStartAt(visit.getStartAt());
        dto.setFinishAt(visit.getFinishAt());
        dto.setStatus(visit.getStatus());
        dto.setCustomer(toRead(visit.getCustomer()));
        dto.setMaster(toRead(visit.getMaster()));
        dto.setCreatedAt(visit.getCreatedAt());
        dto.setUpdatedAt(visit.getUpdatedAt());
        return dto;
    }

}
