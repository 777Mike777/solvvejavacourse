package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Person;
import com.artemjev.backend.example.dto.person.PersonCreateDTO;
import com.artemjev.backend.example.dto.person.PersonPatchDTO;
import com.artemjev.backend.example.dto.person.PersonPutDTO;
import com.artemjev.backend.example.dto.person.PersonReadDTO;
import com.artemjev.backend.example.repository.PersonRepository;
import com.artemjev.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;


    public PersonReadDTO getPerson(UUID id) {
        Person person = repositoryHelper.getEntityRequired(Person.class, id);
        return translationService.toRead(person);
    }

    public PersonReadDTO createPerson(PersonCreateDTO create) {
        Person person = new Person();
        person.setFirstName(create.getFirstName());
        person.setLastName(create.getLastName());
        person.setPhone(create.getPhone());
        person.setSex(create.getSex());
        person.setBirthDay(create.getBirthDay());
        person.setDeathDay(create.getDeathDay());
        person.setMaritalStatus(create.getMaritalStatus());
        person.setChildrenAmount(create.getChildrenAmount());
        person.setNationality(create.getNationality());
        person.setCitizenship(create.getCitizenship());
        person.setResidencePlace(create.getResidencePlace());
        person.setAutobiography(create.getAutobiography());
        person.setAdditionalInfo(create.getAdditionalInfo());

        person = personRepository.save(person);
        return translationService.toRead(person);
    }

    public PersonReadDTO patchPerson(UUID id, PersonPatchDTO patch) {
        Person person = repositoryHelper.getEntityRequired(Person.class, id);

        if (patch.getFirstName() != null) {
            person.setFirstName(patch.getFirstName());
        }
        if (patch.getLastName() != null) {
            person.setLastName(patch.getLastName());
        }
        if (patch.getPhone() != null) {
            person.setPhone(patch.getPhone());
        }
        if (patch.getSex() != null) {
            person.setSex(patch.getSex());
        }
        if (patch.getBirthDay() != null) {
            person.setBirthDay(patch.getBirthDay());
        }
        if (patch.getDeathDay() != null) {
            person.setDeathDay(patch.getDeathDay());
        }
        if (patch.getMaritalStatus() != null) {
            person.setMaritalStatus(patch.getMaritalStatus());
        }
        if (patch.getChildrenAmount() != null) {
            person.setChildrenAmount(patch.getChildrenAmount());
        }
        if (patch.getNationality() != null) {
            person.setNationality(patch.getNationality());
        }
        if (patch.getCitizenship() != null) {
            person.setCitizenship(patch.getCitizenship());
        }
        if (patch.getResidencePlace() != null) {
            person.setResidencePlace(patch.getResidencePlace());
        }
        if (patch.getAutobiography() != null) {
            person.setAutobiography(patch.getAutobiography());
        }
        if (patch.getAdditionalInfo() != null) {
            person.setAdditionalInfo(patch.getAdditionalInfo());
        }
        person = personRepository.save(person);
        return translationService.toRead(person);
    }

    public PersonReadDTO updatePerson(UUID id, PersonPutDTO put) {
        Person person = repositoryHelper.getEntityRequired(Person.class, id);

        person.setFirstName(put.getFirstName());
        person.setLastName(put.getLastName());
        person.setPhone(put.getPhone());
        person.setSex(put.getSex());
        person.setBirthDay(put.getBirthDay());
        person.setDeathDay(put.getDeathDay());
        person.setMaritalStatus(put.getMaritalStatus());
        person.setChildrenAmount(put.getChildrenAmount());
        person.setNationality(put.getNationality());
        person.setCitizenship(put.getCitizenship());
        person.setResidencePlace(put.getResidencePlace());
        person.setAutobiography(put.getAutobiography());
        person.setAdditionalInfo(put.getAdditionalInfo());

        person = personRepository.save(person);
        return translationService.toRead(person);
    }

    public void deletePerson(UUID id) {
        personRepository.delete(repositoryHelper.getEntityRequired(Person.class, id));
    }

}