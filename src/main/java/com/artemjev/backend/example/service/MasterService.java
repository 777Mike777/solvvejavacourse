package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.repository.MasterRepository;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class MasterService {

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageMarkOfMaster(UUID masterId) {
        Double averageMark = visitRepository.calcAverageMarkOfMaster(masterId);
        Master master = masterRepository.findById(masterId).orElseThrow(
                () -> new EntityNotFoundException(Master.class, masterId));

        log.info("Setting new average mark of master: {}. Old value: {}, new value {}", masterId,
                master.getAverageMark(), averageMark);
        master.setAverageMark(averageMark);
        masterRepository.save(master);
    }
}