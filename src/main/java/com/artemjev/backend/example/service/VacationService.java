package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.domain.Vacation;
import com.artemjev.backend.example.dto.vacation.*;
import com.artemjev.backend.example.repository.RepositoryHelper;
import com.artemjev.backend.example.repository.vacation.VacationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class VacationService {

    @Autowired
    private VacationRepository vacationRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;


    public List<VacationReadDTO> getMasterVacations(UUID masterId) {
        VacationFilter filter = new VacationFilter();
        filter.setMasterId(masterId);
        List<Vacation> vacations = vacationRepository.findByFilter(filter);
        return vacations.stream().map(translationService::toRead).collect(Collectors.toList());
    }

    public VacationReadDTO getMasterVacation(UUID masterId, UUID vacationId) {
        VacationFilter filter = new VacationFilter();
        filter.setMasterId(masterId);
        filter.setVacationId(vacationId);
        List<Vacation> vacations = vacationRepository.findByFilter(filter);
        return translationService.toRead(vacations.get(0));
    }

    public VacationReadDTO createVacation(UUID masterId, VacationCreateDTO create) {
        Vacation vacation = translationService.toEntity(create);
        vacation.setMaster(repositoryHelper.getReferenceIfExist(Master.class, masterId));
        vacation = vacationRepository.save(vacation);

        return translationService.toRead(vacation);
    }

    public VacationReadDTO patchMasterVacation(UUID id, VacationPatchDTO patch) {
        Vacation vacation = repositoryHelper.getEntityRequired(Vacation.class, id);

        if (patch.getStartAt() != null) {
            vacation.setStartAt(patch.getStartAt());
        }
        if (patch.getEndAt() != null) {
            vacation.setEndAt(patch.getEndAt());
        }
        if (patch.getMasterId() != null) {
            Master master = repositoryHelper.getEntityRequired(Master.class, patch.getMasterId());
            vacation.setMaster(master);
        }
        vacation = vacationRepository.save(vacation);
        return translationService.toRead(vacation);
    }

    public VacationReadDTO updateMasterVacation(UUID id, VacationPutDTO put) {
        Vacation vacation = repositoryHelper.getEntityRequired(Vacation.class, id);

        vacation.setStartAt(put.getStartAt());
        vacation.setEndAt(put.getEndAt());

        if (put.getMasterId() != null) {
            Master master = repositoryHelper.getEntityRequired(Master.class, put.getMasterId());
            vacation.setMaster(master);
        }
        vacation = vacationRepository.save(vacation);
        return translationService.toRead(vacation);
    }

    public void deleteVacation(UUID id) {
        vacationRepository.delete(repositoryHelper.getEntityRequired(Vacation.class, id));
    }

    public List<VacationReadDTO> getVacations(VacationFilter filter) {
        List<Vacation> vacations = vacationRepository.findByFilter(filter);
        return vacations.stream().map(translationService::toRead).collect(Collectors.toList());
    }

}
