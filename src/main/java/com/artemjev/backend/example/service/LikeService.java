package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Like;
import com.artemjev.backend.example.dto.like.LikeCreateDTO;
import com.artemjev.backend.example.dto.like.LikeFilter;
import com.artemjev.backend.example.dto.like.LikeReadDTO;
import com.artemjev.backend.example.repository.RepositoryHelper;
import com.artemjev.backend.example.repository.like.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class LikeService {

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public LikeReadDTO getLike(UUID id) {
        Like like = repositoryHelper.getEntityRequired(Like.class, id);
        return translationService.toRead(like);
    }

    public List<LikeReadDTO> getLikes(LikeFilter filter) {
        List<Like> likes = likeRepository.findByFilter(filter);
        return likes.stream().map(translationService::toRead).collect(Collectors.toList());
    }

    public LikeReadDTO createLike(LikeCreateDTO create) {
        Like like = translationService.toEntity(create);
        like = likeRepository.save(like);
        return translationService.toRead(like);
    }

}
