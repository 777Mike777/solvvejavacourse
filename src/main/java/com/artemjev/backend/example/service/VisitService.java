package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Visit;
import com.artemjev.backend.example.dto.visit.VisitCreateDTO;
import com.artemjev.backend.example.dto.visit.VisitFilter;
import com.artemjev.backend.example.dto.visit.VisitReadDTO;
import com.artemjev.backend.example.dto.visit.VisitReadExtendedDTO;
import com.artemjev.backend.example.repository.RepositoryHelper;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class VisitService {

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public VisitReadExtendedDTO getVisit(UUID id) {
        Visit visit = repositoryHelper.getEntityRequired(Visit.class, id);
        return translationService.toReadExtended(visit);
    }

    public List<VisitReadDTO> getVisits(VisitFilter filter) {
        List<Visit> visits = visitRepository.findByFilter(filter);
        return visits.stream().map(translationService::toRead).collect(Collectors.toList());
    }

    public VisitReadDTO createVisit(VisitCreateDTO create) {
        Visit visit = translationService.toEntity(create);
        visit = visitRepository.save(visit);
        return translationService.toRead(visit);
    }

}
