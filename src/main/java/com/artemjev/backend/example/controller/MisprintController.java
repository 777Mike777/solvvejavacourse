package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.dto.misprint.MisprintCreateDTO;
import com.artemjev.backend.example.dto.misprint.MisprintFilter;
import com.artemjev.backend.example.dto.misprint.MisprintProcessDTO;
import com.artemjev.backend.example.dto.misprint.MisprintReadDTO;
import com.artemjev.backend.example.service.MisprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/misprints")
public class MisprintController {

    @Autowired
    private MisprintService misprintService;

    @GetMapping("/{id}")
    public MisprintReadDTO getMisprint(@PathVariable UUID id) {
        return misprintService.getMisprint(id);
    }

    @GetMapping
    public List<MisprintReadDTO> getMisprints(MisprintFilter filter) {
        return misprintService.getMisprints(filter);
    }

    @PostMapping
    public MisprintReadDTO createMisprint(@RequestBody MisprintCreateDTO create) {
        return misprintService.createMisprint(create);
    }
//
//    @PostMapping
//    public MisprintReadDTO createMisprint(@RequestBody MisprintCreateDTO create) {
//        return misprintService.createMisprint(create);
//    }

    @PostMapping("/{id}/to-process")
    public MisprintReadDTO ProcessMisprint(@PathVariable UUID misprintId,
                                           @RequestBody MisprintProcessDTO process) {
        misprintService.processMisprint(process);
        return null;
    }
}