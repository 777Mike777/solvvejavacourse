package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.dto.person.PersonCreateDTO;
import com.artemjev.backend.example.dto.person.PersonPatchDTO;
import com.artemjev.backend.example.dto.person.PersonPutDTO;
import com.artemjev.backend.example.dto.person.PersonReadDTO;
import com.artemjev.backend.example.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@RestController
@RequestMapping("/api/v1/persons")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/{id}")
    public PersonReadDTO getPerson(@PathVariable UUID id) {
        return personService.getPerson(id);
    }

    @PostMapping
    public PersonReadDTO createPerson(@RequestBody PersonCreateDTO createDTO) {
        return personService.createPerson(createDTO);
    }

    @PatchMapping("/{id}")
    public PersonReadDTO patchPerson(@PathVariable UUID id, @RequestBody PersonPatchDTO patch) {
        return personService.patchPerson(id, patch);
    }

    @PutMapping("/{id}")
    public PersonReadDTO putPerson(@PathVariable UUID id, @RequestBody PersonPutDTO put) {
        return personService.updatePerson(id, put);
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable UUID id) {
        personService.deletePerson(id);
    }
}
