package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.dto.customer.CustomerCreateDTO;
import com.artemjev.backend.example.dto.customer.CustomerPatchDTO;
import com.artemjev.backend.example.dto.customer.CustomerPutDTO;
import com.artemjev.backend.example.dto.customer.CustomerReadDTO;
import com.artemjev.backend.example.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/{id}")
    public CustomerReadDTO getCustomer(@PathVariable UUID id) {
        CustomerReadDTO customerReadDTO = customerService.getCustomer(id);
        return customerReadDTO;
    }

    @PostMapping
    public CustomerReadDTO createCustomer(@RequestBody CustomerCreateDTO createDTO) {
        return customerService.createCustomer(createDTO);
    }

    @PatchMapping("/{id}")
    public CustomerReadDTO patchCustomer(@PathVariable UUID id, @RequestBody CustomerPatchDTO patch) {
        return customerService.patchCustomer(id, patch);
    }

    @PutMapping("/{id}")
    public CustomerReadDTO putCustomer(@PathVariable UUID id, @RequestBody CustomerPutDTO put) {
        return customerService.updateCustomer(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable UUID id) {
        customerService.deleteCustomer(id);
    }
}