package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.dto.complaintobject.*;
import com.artemjev.backend.example.service.ComplaintObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/complaints/{complaintId}/complaint-objects")
public class ComplaintObjectController {

    @Autowired
    private ComplaintObjectService complaintObjectService;

    @GetMapping("/{id}")
    public ComplaintObjectReadDTO getComplaintObject(@PathVariable UUID complaintId, @PathVariable UUID id) {
        return complaintObjectService.getComplaintObject(complaintId, id);
    }

    @GetMapping
    public List<ComplaintObjectReadDTO> getComplaintObjects(@PathVariable UUID complaintId,
                                                            ComplaintObjectFilter filter) {
        return complaintObjectService.getComplaintObjects(complaintId, filter);
    }

    @PostMapping
    public ComplaintObjectReadDTO createComplaintObject(@PathVariable UUID complaintId,
                                                        @RequestBody ComplaintObjectCreateDTO create) {
        return complaintObjectService.createComplaintObject(complaintId, create);
    }

    @PatchMapping("/{id}")
    public ComplaintObjectReadDTO patchComplaintComplaintObject(@PathVariable UUID complaintId, @PathVariable UUID id,
                                                                @RequestBody ComplaintObjectPatchDTO patch) {
        return complaintObjectService.patchComplaintObject(complaintId, id, patch);
    }

    @PutMapping("/{id}")
    public ComplaintObjectReadDTO putComplaintComplaintObjects(@PathVariable UUID complaintId, @PathVariable UUID id,
                                                               @RequestBody ComplaintObjectPutDTO put) {
        return complaintObjectService.updateComplaintObject(complaintId, id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable UUID complaintId, @PathVariable UUID id) {
        complaintObjectService.deleteComplaintObject(complaintId, id);
    }

}
