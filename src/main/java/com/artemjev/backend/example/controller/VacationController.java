package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.dto.vacation.VacationCreateDTO;
import com.artemjev.backend.example.dto.vacation.VacationPatchDTO;
import com.artemjev.backend.example.dto.vacation.VacationPutDTO;
import com.artemjev.backend.example.dto.vacation.VacationReadDTO;
import com.artemjev.backend.example.service.VacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/masters/{masterId}/vacations")
public class VacationController {

    @Autowired
    private VacationService vacationService;

    @GetMapping("/{id}")
    public VacationReadDTO getMasterVacation(@PathVariable UUID masterId, @PathVariable UUID id) {
        return vacationService.getMasterVacation(masterId, id);
    }

    @GetMapping
    public List<VacationReadDTO> getMasterVacations(@PathVariable UUID masterId) {
        return vacationService.getMasterVacations(masterId);
    }

    @PostMapping
    public VacationReadDTO createVacation(@PathVariable UUID masterId,
                                          @RequestBody VacationCreateDTO create) {
        return vacationService.createVacation(masterId, create);
    }

    @PatchMapping("/{id}")
    public VacationReadDTO patchMasterVacation(@PathVariable UUID masterId, @PathVariable UUID id,
                                               @RequestBody VacationPatchDTO patch) {
        return vacationService.patchMasterVacation(id, patch);
    }

    @PutMapping("/{id}")
    public VacationReadDTO putMasterVacations(@PathVariable UUID masterId, @PathVariable UUID id,
                                              @RequestBody VacationPutDTO put) {
        return vacationService.updateMasterVacation(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable UUID id) {
        vacationService.deleteVacation(id);
    }
}
