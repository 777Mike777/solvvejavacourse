package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.dto.visit.VisitCreateDTO;
import com.artemjev.backend.example.dto.visit.VisitFilter;
import com.artemjev.backend.example.dto.visit.VisitReadDTO;
import com.artemjev.backend.example.dto.visit.VisitReadExtendedDTO;
import com.artemjev.backend.example.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/visits")
public class VisitController {

    @Autowired
    private VisitService visitService;

    @GetMapping("/{id}")
    public VisitReadExtendedDTO getVisit(@PathVariable UUID id) {
        return visitService.getVisit(id);
    }

    @GetMapping
    public List<VisitReadDTO> getVisits(VisitFilter filter) {
        return visitService.getVisits(filter);
    }

    @PostMapping
    public VisitReadDTO createVisit(@RequestBody VisitCreateDTO create) {
        return visitService.createVisit(create);
    }
}