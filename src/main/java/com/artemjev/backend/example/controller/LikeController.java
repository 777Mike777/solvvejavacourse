package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.dto.like.LikeCreateDTO;
import com.artemjev.backend.example.dto.like.LikeFilter;
import com.artemjev.backend.example.dto.like.LikeReadDTO;
import com.artemjev.backend.example.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/likes")
public class LikeController {

    @Autowired
    private LikeService likeService;

    @GetMapping("/{id}")
    public LikeReadDTO getLike(@PathVariable UUID id) {
        return likeService.getLike(id);
    }

    @GetMapping
    public List<LikeReadDTO> getLikes(LikeFilter filter) {
        return likeService.getLikes(filter);
    }

    @PostMapping
    public LikeReadDTO createLike(@RequestBody LikeCreateDTO create) {
        return likeService.createLike(create);
    }
}