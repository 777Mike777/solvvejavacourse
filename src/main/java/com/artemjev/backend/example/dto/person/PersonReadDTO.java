package com.artemjev.backend.example.dto.person;

import com.artemjev.backend.example.domain.Sex;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;


@Data
public class PersonReadDTO {
    private UUID id;
    private String firstName;
    private String lastName;
    private String phone;
    private Sex sex;
    private LocalDate birthDay;
    private LocalDate deathDay;
    private String maritalStatus;
    private Byte childrenAmount;
    private String nationality;
    private String citizenship;
    private String residencePlace;
    private String autobiography;
    private String additionalInfo;
    private Instant createdAt;
    private Instant updatedAt;
}
