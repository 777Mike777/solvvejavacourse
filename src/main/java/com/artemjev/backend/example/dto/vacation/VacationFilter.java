package com.artemjev.backend.example.dto.vacation;

import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;


@Data
public class VacationFilter {

    //    TODO: vacationId в фильтре лишнее, как мне сейчас кажется. Переделать без этого поля!
    //     Ранее решил использовать поиск по фильтру в теле метода getMasterVacation(UUID masterId, UUID vacationId),
    //     что на текущий момент кажеться мне неправильным и в  др.сущностях делал иначе.
    private UUID vacationId;
    private UUID masterId;

    //    TODO: нужно что бы для каждого посля с типом дата/время в фильтре задавался интрвал,
    //     т.е. на каждое такое поле должен быть определен диапазон значений, как промежуток.
    //     Затупил когда-то, нужно исправить!
    private LocalDate startAt;
    private LocalDate endAt;
}

