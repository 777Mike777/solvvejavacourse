package com.artemjev.backend.example.dto.visit;

import com.artemjev.backend.example.domain.VisitStatus;
import com.artemjev.backend.example.dto.customer.CustomerReadDTO;
import com.artemjev.backend.example.dto.MasterReadDTO;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class VisitReadExtendedDTO {
    private UUID id;
    private CustomerReadDTO customer;
    private MasterReadDTO master;
    private Instant startAt;
    private Instant finishAt;
    private VisitStatus status;
    private Instant createdAt;
    private Instant updatedAt;
}
