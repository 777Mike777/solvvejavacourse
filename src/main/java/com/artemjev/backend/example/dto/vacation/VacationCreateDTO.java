package com.artemjev.backend.example.dto.vacation;

import lombok.Data;
import java.time.LocalDate;

@Data
public class VacationCreateDTO {
    private LocalDate startAt;
    private LocalDate endAt;
}
