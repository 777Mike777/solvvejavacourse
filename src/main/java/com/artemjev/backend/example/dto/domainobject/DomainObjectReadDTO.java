package com.artemjev.backend.example.dto.domainobject;

import com.artemjev.backend.example.domain.DomainObjectType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class DomainObjectReadDTO {

    private UUID id;
    private DomainObjectType objectType;
    private UUID objectId;
    private Instant createdAt;
    private Instant updatedAt;
}
