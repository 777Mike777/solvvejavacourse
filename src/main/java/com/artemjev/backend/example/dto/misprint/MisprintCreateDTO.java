package com.artemjev.backend.example.dto.misprint;

import lombok.Data;
import java.util.UUID;


@Data
public class MisprintCreateDTO {

    private UUID misprintObjectId;
    private String misprintObjectFieldName;
    private String actualFieldValue;
    private String suggestedFieldValue;
}
