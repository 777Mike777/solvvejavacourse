package com.artemjev.backend.example.dto.complaintobject;

import com.artemjev.backend.example.domain.DomainObjectType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class ComplaintObjectFilter {

    private DomainObjectType objectType;
    private UUID objectId;
    private String objectFieldName;
    private String note;
    private Instant createdAtFrom;
    private Instant createdAtTo;
    private Instant updatedAtFrom;
    private Instant updatedAtTo;
}

