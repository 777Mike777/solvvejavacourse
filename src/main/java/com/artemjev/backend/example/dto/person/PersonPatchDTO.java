package com.artemjev.backend.example.dto.person;

import com.artemjev.backend.example.domain.Sex;
import lombok.Data;

import java.time.LocalDate;


@Data
public class PersonPatchDTO {
    private String firstName;
    private String lastName;
    private String phone;
    private Sex sex;
    private LocalDate birthDay;
    private LocalDate deathDay;
    private String maritalStatus;
    private Byte childrenAmount;
    private String nationality;
    private String citizenship;
    private String residencePlace;
    private String autobiography;
    private String additionalInfo;
}
