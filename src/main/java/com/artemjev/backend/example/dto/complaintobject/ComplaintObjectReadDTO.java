package com.artemjev.backend.example.dto.complaintobject;

import com.artemjev.backend.example.domain.DomainObjectType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class ComplaintObjectReadDTO {

    private UUID id;
    private UUID complaintId;
    private DomainObjectType objectType;
    private UUID objectId;
    private String objectFieldName;
    private String note;
    private Instant createdAt;
    private Instant updatedAt;
}

