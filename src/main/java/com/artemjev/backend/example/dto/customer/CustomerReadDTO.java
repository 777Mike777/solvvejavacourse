package com.artemjev.backend.example.dto.customer;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class CustomerReadDTO {

    private UUID id;
    private String name;
    private String phone;
    private Instant createdAt;
    private Instant updatedAt;
}
