package com.artemjev.backend.example.dto.misprint;

import com.artemjev.backend.example.domain.MisprintStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class MisprintReadDTO {

    private UUID id;
    private UUID userSuppliantId;
    private MisprintStatus status;
    private UUID misprintObjectId;
    private String misprintObjectFieldName;
    private String actualFieldValue;
    private String suggestedFieldValue;
    private UUID correctedByContentManagerId;
    private Instant correctedAt;
    private Instant createdAt;
    private Instant updatedAt;
}
