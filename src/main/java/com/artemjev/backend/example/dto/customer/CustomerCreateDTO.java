package com.artemjev.backend.example.dto.customer;

import lombok.Data;

@Data
public class CustomerCreateDTO {
    private String name;
    private String phone;
}
