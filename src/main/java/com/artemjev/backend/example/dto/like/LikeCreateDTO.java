package com.artemjev.backend.example.dto.like;

import com.artemjev.backend.example.domain.DomainObjectType;
import lombok.Data;

import java.util.UUID;


@Data
public class LikeCreateDTO {
    private Boolean like;
    private UUID userId;
    private DomainObjectType likedObjectType;
    private UUID likedObjectId;
}
