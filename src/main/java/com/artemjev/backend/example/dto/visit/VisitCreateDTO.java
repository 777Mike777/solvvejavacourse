package com.artemjev.backend.example.dto.visit;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class VisitCreateDTO {
    private UUID customerId;
    private UUID masterId;

    private Instant startAt;
    private Instant finishAt;
}
