package com.artemjev.backend.example.dto.complaint;

import com.artemjev.backend.example.domain.*;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class ComplaintReadDTO {

    private UUID id;
    private ComplaintStatus status;
    private UUID userId;
    private ComplaintTargType targetType;
    private String complaintMessage;
    private Instant createdAt;
    private Instant updatedAt;

}
