package com.artemjev.backend.example.dto.misprint;

import com.artemjev.backend.example.domain.VisitStatus;
import lombok.Data;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;


@Data
public class MisprintFilter {

    private UUID userSuppliantId;
    private Set<VisitStatus> statuses;
    private UUID misprintObjectId;
    private String misprintObjectFieldName;
    private String actualFieldValue;
    private String suggestedFieldValue;
    private UUID correctedByContentManagerId;
    private Instant correctedAtFrom;
    private Instant correctedAtTo;
}

