package com.artemjev.backend.example.dto.complaintobject;

import com.artemjev.backend.example.domain.DomainObjectType;
import lombok.Data;

import java.util.UUID;


@Data
public class ComplaintObjectPatchDTO {

    private UUID complaintId;
    private DomainObjectType objectType;
    private UUID objectId;
    private String objectFieldName;
    private String note;
}

