package com.artemjev.backend.example.dto.vacation;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;


@Data
public class VacationReadDTO {

    private UUID id;
    private UUID masterId;
    private LocalDate startAt;
    private LocalDate endAt;
    private Instant createdAt;
    private Instant updatedAt;
}
