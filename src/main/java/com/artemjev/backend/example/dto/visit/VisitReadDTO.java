package com.artemjev.backend.example.dto.visit;

import com.artemjev.backend.example.domain.VisitStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class VisitReadDTO {
    private UUID id;
    private UUID customerId;
    private UUID masterId;
    private Instant startAt;
    private Instant finishAt;
    private VisitStatus status;
    private Integer customerMark;
    private Instant createdAt;
    private Instant updatedAt;
}
