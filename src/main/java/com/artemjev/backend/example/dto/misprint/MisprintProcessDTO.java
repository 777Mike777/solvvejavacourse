package com.artemjev.backend.example.dto.misprint;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MisprintProcessDTO {

    private String misprintObjectFieldName;
    private String actualFieldValue;
    private String suggestedFieldValue;
    private UUID correctedByContentManagerId;
    private Instant correctedAt;
    private Instant createdAt;
    private Instant updatedAt;
}
