package com.artemjev.backend.example.dto.vacation;

import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;


@Data
public class VacationPutDTO {

    private UUID masterId;
    private LocalDate startAt;
    private LocalDate endAt;
}
