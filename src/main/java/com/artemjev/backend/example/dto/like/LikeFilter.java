package com.artemjev.backend.example.dto.like;

import com.artemjev.backend.example.domain.DomainObjectType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class LikeFilter {
    private Boolean like;
    private UUID userId;
    private DomainObjectType likedObjectType;
    private UUID likedObjectId;
    private Boolean isActive;
    private Instant createdAtFrom;
    private Instant createdAtTo;
    private Instant updatedAtFrom;
    private Instant updatedAtTo;
}

