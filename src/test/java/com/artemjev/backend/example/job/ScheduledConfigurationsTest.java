package com.artemjev.backend.example.job;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ScheduledConfigurationsTest.ScheduledConfig.class) // только для єтого теста включаем конфигурацию скедулера
@ActiveProfiles("test")
public class ScheduledConfigurationsTest {



    @Test
    public void testSpringContextUpAndRunning() {
        log.info("@Scheduled configuration are OK");
    }

    @EnableScheduling
    static class ScheduledConfig {
    }





}
