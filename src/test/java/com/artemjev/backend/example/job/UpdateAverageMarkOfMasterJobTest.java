package com.artemjev.backend.example.job;

import com.artemjev.backend.example.domain.Customer;
import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.domain.Visit;
import com.artemjev.backend.example.domain.VisitStatus;
import com.artemjev.backend.example.repository.CustomerRepository;
import com.artemjev.backend.example.repository.MasterRepository;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import com.artemjev.backend.example.service.MasterService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {
        "delete from visit",
        "delete from customer",
        "delete from master"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UpdateAverageMarkOfMasterJobTest {


    @Autowired
    private UpdateAverageMarkOfMasterJob updateAverageMarkOfMasterJob;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private VisitRepository visitRepository;

    @SpyBean
    private MasterService masterService;


    @Test
    public void testUpdateAverageMarkOfMasters() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();

        createVisit(c1, m1, 3);
        createVisit(c1, m1, 5);
        updateAverageMarkOfMasterJob.updateAverageMarkOfMasters();

        m1 = masterRepository.findById(m1.getId()).get();
        Assert.assertEquals(4.0, m1.getAverageMark(), Double.MIN_NORMAL);
    }

    @Test
    //  @Transactional(noRollbackFor = RuntimeException.class) //??????
    public void testMastersUpdatedIndependently() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();
        Master m2 = createMaster();

        createVisit(c1, m1, 4);
        createVisit(c1, m2, 5);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(masterService).updateAverageMarkOfMaster(Mockito.any());

        updateAverageMarkOfMasterJob.updateAverageMarkOfMasters();

        for (Master m : masterRepository.findAll()) {
            if (m.getId().equals(failedId[0])) {
                Assert.assertNull(m.getAverageMark());
            } else {
                Assert.assertNotNull(m.getAverageMark());
            }
        }
    }


    //***************************************************
    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("Joe");
        customer.setPhone("1234");
        System.out.println("Customer was created " + customer);
        return customerRepository.save(customer);
    }

    private Master createMaster() {
        Master master = new Master();
        master.setName("Qqq");
        master.setPhone("777");
        master.setAbout("The best master");
        return masterRepository.save(master);
    }

    private Visit createVisit(Customer customer, Master master, Integer customerMark) {
        Visit visit = new Visit();
        visit.setCustomer(customer);
        visit.setMaster(master);
        visit.setCustomerMark(customerMark);
        visit.setStatus(VisitStatus.SCHEDULED);
        visit.setStartAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        return visitRepository.save(visit);
    }
}

