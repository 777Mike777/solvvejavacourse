package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Person;
import com.artemjev.backend.example.domain.Sex;
import com.artemjev.backend.example.dto.person.PersonCreateDTO;
import com.artemjev.backend.example.dto.person.PersonPatchDTO;
import com.artemjev.backend.example.dto.person.PersonPutDTO;
import com.artemjev.backend.example.dto.person.PersonReadDTO;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.repository.PersonRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.UUID;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements =
        {"delete from actor",
                "delete from person"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class PersonServiceTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @Test
    public void testGetPerson() {
        Person person = createPerson();
        PersonReadDTO read = personService.getPerson(person.getId());
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetPersonWrongId() {
        personService.getPerson(UUID.randomUUID());
    }

    @Test
    public void testCreatePerson() {
        PersonCreateDTO create = new PersonCreateDTO();
        create.setFirstName("Joe");
        create.setLastName("First");
        create.setPhone("123");
        create.setSex(Sex.UNDEFINED);
        create.setBirthDay(LocalDate.parse("2000-10-01"));
        create.setDeathDay(null);
        create.setMaritalStatus("bachelor");
        create.setChildrenAmount((byte) 0);
        create.setNationality("Reptiloid");
        create.setCitizenship("Yankee");
        create.setResidencePlace("Ukraine");
        create.setAutobiography("There is no data");
        create.setAdditionalInfo("There is no data");

        PersonReadDTO read = personService.createPerson(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Person person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPerson() {
        Person person = createPerson();

        PersonPatchDTO patch = new PersonPatchDTO();
        patch.setFirstName("Joe");
        patch.setLastName("First");
        patch.setPhone("123");
        patch.setSex(Sex.UNDEFINED);
        patch.setBirthDay(LocalDate.parse("2000-10-01"));
        patch.setDeathDay(null);
        patch.setMaritalStatus("bachelor");
        patch.setChildrenAmount((byte) 0);
        patch.setNationality("Reptiloid");
        patch.setCitizenship("Yankee");
        patch.setResidencePlace("Ukraine");
        patch.setAutobiography("There is no data");
        patch.setAdditionalInfo("There is no data");

        PersonReadDTO read = personService.patchPerson(person.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPersonEmptyPatch() {
        Person person = createPerson();
        PersonPatchDTO patch = new PersonPatchDTO();

        PersonReadDTO read = personService.patchPerson(person.getId(), patch);
        Assert.assertNotNull(read.getFirstName());
        Assert.assertNotNull(read.getLastName());
        Assert.assertNotNull(read.getPhone());
        Assert.assertNotNull(read.getSex());
        Assert.assertNotNull(read.getBirthDay());
        Assert.assertNotNull(read.getMaritalStatus());
        Assert.assertNotNull(read.getChildrenAmount());
        Assert.assertNotNull(read.getNationality());
        Assert.assertNotNull(read.getCitizenship());
        Assert.assertNotNull(read.getResidencePlace());
        Assert.assertNotNull(read.getAutobiography());
        Assert.assertNotNull(read.getAdditionalInfo());

        Person personAfterUpdate = personRepository.findById(read.getId()).get();
        Assert.assertNotNull(personAfterUpdate.getFirstName());
        Assert.assertNotNull(personAfterUpdate.getLastName());
        Assert.assertNotNull(personAfterUpdate.getPhone());
        Assert.assertNotNull(personAfterUpdate.getSex());
        Assert.assertNotNull(personAfterUpdate.getBirthDay());
        Assert.assertNotNull(personAfterUpdate.getMaritalStatus());
        Assert.assertNotNull(personAfterUpdate.getChildrenAmount());
        Assert.assertNotNull(personAfterUpdate.getNationality());
        Assert.assertNotNull(personAfterUpdate.getCitizenship());
        Assert.assertNotNull(personAfterUpdate.getResidencePlace());
        Assert.assertNotNull(personAfterUpdate.getAutobiography());
        Assert.assertNotNull(personAfterUpdate.getAdditionalInfo());
        Assertions.assertThat(person).isEqualToComparingFieldByField(personAfterUpdate);
    }

    @Test
    public void testUpdatePerson() {
        Person person = createPerson();
        PersonPutDTO put = new PersonPutDTO();
        put.setFirstName("Joe");
        put.setLastName("First");
        put.setPhone("123");
        put.setSex(Sex.UNDEFINED);
        put.setBirthDay(LocalDate.parse("2000-10-01"));
        put.setDeathDay(null);
        put.setMaritalStatus("bachelor");
        put.setChildrenAmount((byte) 0);
        put.setNationality("Reptiloid");
        put.setCitizenship("Yankee");
        put.setResidencePlace("Ukraine");
        put.setAutobiography("There is no data");
        put.setAdditionalInfo("There is no data");

        PersonReadDTO read = personService.updatePerson(person.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testUpdatePersonEmptyUpdate() {
        Person person = createPerson();
        PersonPutDTO put = new PersonPutDTO();

        PersonReadDTO read = personService.updatePerson(person.getId(), put);
        Assert.assertNull(read.getFirstName());
        Assert.assertNull(read.getLastName());
        Assert.assertNull(read.getPhone());
        Assert.assertNull(read.getSex());
        Assert.assertNull(read.getBirthDay());
        Assert.assertNull(read.getMaritalStatus());
        Assert.assertNull(read.getChildrenAmount());
        Assert.assertNull(read.getNationality());
        Assert.assertNull(read.getCitizenship());
        Assert.assertNull(read.getResidencePlace());
        Assert.assertNull(read.getAutobiography());
        Assert.assertNull(read.getAdditionalInfo());

        Person personAfterUpdate = personRepository.findById(read.getId()).get();
        Assert.assertNull(personAfterUpdate.getFirstName());
        Assert.assertNull(personAfterUpdate.getLastName());
        Assert.assertNull(personAfterUpdate.getPhone());
        Assert.assertNull(personAfterUpdate.getSex());
        Assert.assertNull(personAfterUpdate.getBirthDay());
        Assert.assertNull(personAfterUpdate.getMaritalStatus());
        Assert.assertNull(personAfterUpdate.getChildrenAmount());
        Assert.assertNull(personAfterUpdate.getNationality());
        Assert.assertNull(personAfterUpdate.getCitizenship());
        Assert.assertNull(personAfterUpdate.getResidencePlace());
        Assert.assertNull(personAfterUpdate.getAutobiography());
        Assert.assertNull(personAfterUpdate.getAdditionalInfo());
    }

    @Test
    public void testDeletePerson() {
        Person person = createPerson();

        personService.deletePerson(person.getId());
        Assert.assertFalse(personRepository.existsById(person.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeletePersonNotFound() {
        personService.deletePerson(UUID.randomUUID());
    }

    //*******private-methods****************
    private Person createPerson() {
        Person person = new Person();
        person.setId(UUID.randomUUID());
        person.setFirstName("Joe");
        person.setLastName("First");
        person.setPhone("123");
        person.setSex(Sex.UNDEFINED);
        person.setBirthDay(LocalDate.parse("2000-10-01"));
        person.setDeathDay(null);
        person.setMaritalStatus("bachelor");
        person.setChildrenAmount((byte) 0);
        person.setNationality("Reptiloid");
        person.setCitizenship("Yankee");
        person.setResidencePlace("Ukraine");
        person.setAutobiography("Some kind of autobiography");
        person.setAdditionalInfo("Some additional information");
        return personRepository.save(person);
    }
}