package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Customer;
import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.domain.Visit;
import com.artemjev.backend.example.domain.VisitStatus;
import com.artemjev.backend.example.repository.CustomerRepository;
import com.artemjev.backend.example.repository.MasterRepository;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {
        "delete from visit",
        "delete from customer",
        "delete from master"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)

public class MasterServiceTest {

    @Autowired
    private MasterService masterService;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CustomerRepository customerRepository;


    @Test
    public void testUpdateAverageMarkOfMaster() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();

        createVisit(c1, m1, 3);
        createVisit(c1, m1, 5);

        masterService.updateAverageMarkOfMaster(m1.getId());
        m1 = masterRepository.findById(m1.getId()).get();
        System.out.println( m1);
        Assert.assertEquals(4.0, m1.getAverageMark(), Double.MIN_NORMAL);
    }

    //***************************************************
    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("Joe");
        customer.setPhone("1234");
        System.out.println("Customer was created " + customer);
        return customerRepository.save(customer);
    }

    private Master createMaster() {
        Master master = new Master();
        master.setName("Qqq");
        master.setPhone("777");
        master.setAbout("The best master");
        return masterRepository.save(master);
    }

    //       private Visit createVisit() {
//        Customer c = createCustomer();
//        Master m = createMaster();
//        Visit visit = createVisit(c, m, VisitStatus.SCHEDULED,
//                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));
//        return visitRepository.save(visit);
//    }

    private Visit createVisit(Customer customer, Master master, Integer customerMark) {
        Visit visit = new Visit();
        visit.setCustomer(customer);
        visit.setMaster(master);
        visit.setCustomerMark(customerMark);
        visit.setStatus(VisitStatus.SCHEDULED);
        visit.setStartAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        return visitRepository.save(visit);
    }

//    private Visit createVisit(Customer customer, Master master, VisitStatus visitStatus) {
//        Visit visit = new Visit();
//        visit.setStartAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
//        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
//        visit.setStatus(visitStatus);
//        visit.setCustomer(customer);
//        visit.setMaster(master);
//        return visitRepository.save(visit);
//    }
//
//    private Visit createVisit(Customer customer, Master master, VisitStatus visitStatus, Instant startAt) {
//        Visit visit = new Visit();
//        visit.setStartAt(startAt);
//        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
//        visit.setStatus(visitStatus);
//        visit.setCustomer(customer);
//        visit.setMaster(master);
//        visit.setCustomerMark((int) (Math.random() * 100));
//        return visitRepository.save(visit);
//    }
}
