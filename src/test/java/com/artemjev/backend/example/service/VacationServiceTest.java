package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.domain.Vacation;
import com.artemjev.backend.example.dto.vacation.*;
import com.artemjev.backend.example.repository.MasterRepository;
import com.artemjev.backend.example.repository.vacation.VacationRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Random;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {
        "delete from vacation",
        "delete from master"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class VacationServiceTest {

    @Autowired
    private VacationRepository vacationRepository;

    @Autowired
    private VacationService vacationService;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private TranslationService translationService;

    private Random random = new Random();

    @Test
    public void testGetVacationsByMaster() {
        Master m1 = createMaster();
        Master m2 = createMaster();

        createVacation(m1);
        Vacation v2 = createVacation(m2);
        Vacation v3 = createVacation(m2);

        VacationFilter filter = new VacationFilter();
        filter.setMasterId(m2.getId());
        Assertions.assertThat(vacationService.getMasterVacations(m2.getId())).extracting("id")
                .containsExactlyInAnyOrder(v2.getId(), v3.getId());
    }

    @Test
    public void testCreateVacation() {
        Master master = createMaster();

        VacationCreateDTO create = new VacationCreateDTO();
        create.setStartAt(LocalDate.now().plusDays(random.nextInt(7)));
        create.setEndAt(create.getStartAt().plusDays(37));

        VacationReadDTO read = vacationService.createVacation(master.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Vacation vacation = vacationRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(vacation, "masterId");
    }

    @Test
    public void testPatchVacation() {
        Master master = createMaster();
        Vacation vacation = createVacation(master);

        VacationPatchDTO patch = new VacationPatchDTO();
        patch.setMasterId(master.getId());
        patch.setStartAt(vacation.getStartAt().plusDays(1));
        patch.setEndAt(vacation.getEndAt().plusDays(2));

        VacationReadDTO read = vacationService.patchMasterVacation(vacation.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        vacation = vacationRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(vacation, "masterId");//???
    }

    @Test
    public void testPatchVacationEmptyPatch() {
        Master master = createMaster();
        Vacation vacation = createVacation(master);

        VacationPatchDTO patch = new VacationPatchDTO();

        VacationReadDTO read = vacationService.patchMasterVacation(vacation.getId(), patch);
        Assert.assertNotNull(read.getMasterId());
        Assert.assertNotNull(read.getStartAt());
        Assert.assertNotNull(read.getEndAt());

        Vacation vacationAfterUpdate = vacationRepository.findById(read.getId()).get();
        Assertions.assertThat(vacation).isEqualToIgnoringGivenFields(vacationAfterUpdate, "master");
    }

    @Test
    public void testUpdateVacation() {
        Master master = createMaster();
        Vacation vacation = createVacation(master);

        VacationPutDTO put = new VacationPutDTO();
        put.setMasterId(master.getId());
        put.setStartAt(vacation.getStartAt().plusDays(1));
        put.setEndAt(vacation.getEndAt().plusDays(2));

        VacationReadDTO read = vacationService.updateMasterVacation(vacation.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        vacation = vacationRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(vacation, "masterId");
    }

    @Test
    public void testUpdateVacationEmptyUpdate() {
        Master master = createMaster();
        Vacation vacation = createVacation(master);

        VacationPutDTO put = new VacationPutDTO();

        VacationReadDTO read = vacationService.updateMasterVacation(vacation.getId(), put);
        Assert.assertNotNull(read.getMasterId());
        Assert.assertNull(read.getStartAt());
        Assert.assertNull(read.getEndAt());

        Vacation vacationAfterUpdate = vacationRepository.findById(read.getId()).get();
        Assert.assertNotNull(vacationAfterUpdate.getMaster());
        Assert.assertNull(vacationAfterUpdate.getStartAt());
        Assert.assertNull(vacationAfterUpdate.getEndAt());
    }

    //***************************************************
    private Master createMaster() {
        Master master = new Master();
        master.setName("Qqq");
        master.setPhone("777");
        master.setAbout("The best master");
        return masterRepository.save(master);
    }

    private Vacation createVacation(Master master) {
        Vacation vacation = new Vacation();
        vacation.setStartAt(LocalDate.now().plusDays(random.nextInt(7)));
        vacation.setEndAt(vacation.getStartAt().plusDays(37));
        vacation.setMaster(master);
        return vacationRepository.save(vacation);
    }
}
