package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.*;
import com.artemjev.backend.example.dto.visit.VisitCreateDTO;
import com.artemjev.backend.example.dto.visit.VisitFilter;
import com.artemjev.backend.example.dto.visit.VisitReadDTO;
import com.artemjev.backend.example.dto.visit.VisitReadExtendedDTO;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.repository.CustomerRepository;
import com.artemjev.backend.example.repository.MasterRepository;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.UUID;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {
        "delete from visit",
        "delete from customer",
        "delete from master"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class VisitServiceTest {

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private VisitService visitService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private TranslationService translationService;

    @Test
    public void testGetVisitExtended() {
        Customer customer = createCustomer();
        Master master = createMaster();
        Visit visit = createVisit(customer, master);

        VisitReadExtendedDTO read = visitService.getVisit(visit.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(visit, "customer", "master");
        Assertions.assertThat(read.getCustomer()).isEqualToComparingFieldByField(customer);
        Assertions.assertThat(read.getMaster()).isEqualToComparingFieldByField(master);
    }

    @Test
    public void testGetVisitsWithEmptyFilter() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();
        Customer c2 = createCustomer();
        Master m2 = createMaster();

        Visit v1 = createVisit(c1, m1);
        Visit v2 = createVisit(c1, m2);
        Visit v3 = createVisit(c2, m2);

        VisitFilter filter = new VisitFilter();

        Assertions.assertThat(visitService.getVisits(filter)).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v2.getId(), v3.getId());
    }

    @Test
    public void testGetVisitsByCustomer() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();
        Customer c2 = createCustomer();
        Master m2 = createMaster();

        Visit v1 = createVisit(c1, m1);
        Visit v2 = createVisit(c1, m2);
        createVisit(c2, m2);

        VisitFilter filter = new VisitFilter();
        filter.setCustomerId(c1.getId());
        Assertions.assertThat(visitService.getVisits(filter)).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetVisitsByMaster() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();
        Customer c2 = createCustomer();
        Master m2 = createMaster();

        createVisit(c1, m1);
        Visit v2 = createVisit(c1, m2);
        Visit v3 = createVisit(c2, m2);

        VisitFilter filter = new VisitFilter();
        filter.setMasterId(m2.getId());
        Assertions.assertThat(visitService.getVisits(filter)).extracting("id")
                .containsExactlyInAnyOrder(v2.getId(), v3.getId());
    }

    @Test
    public void testGetVisitsByStatuses() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();
        Customer c2 = createCustomer();
        Master m2 = createMaster();

        createVisit(c1, m1, createInstatn(12), VisitStatus.SCHEDULED);
        Visit v2 = createVisit(c1, m2, createInstatn(13), VisitStatus.CANCELLED);
        Visit v3 = createVisit(c2, m2, createInstatn(9), VisitStatus.FINISHED);

        VisitFilter filter = new VisitFilter();
        filter.setStatuses(Set.of(VisitStatus.CANCELLED, VisitStatus.FINISHED));
        Assertions.assertThat(visitService.getVisits(filter)).extracting("id")
                .containsExactlyInAnyOrder(v2.getId(), v3.getId());
    }

    @Test
    public void testGetVisitsByStartAtInterval() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();
        Customer c2 = createCustomer();
        Master m2 = createMaster();

        Visit v1 = createVisit(c1, m1, createInstatn(12), VisitStatus.SCHEDULED);
        createVisit(c1, m2, createInstatn(13), VisitStatus.CANCELLED);
        Visit v3 = createVisit(c2, m2, createInstatn(9), VisitStatus.FINISHED);

        VisitFilter filter = new VisitFilter();
        filter.setStartAtFrom(createInstatn(9));
        filter.setStartAtTo(createInstatn(13));
        Assertions.assertThat(visitService.getVisits(filter)).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v3.getId());
    }

    @Test
    public void testGetVisitsByAllFilters() {
        Customer c1 = createCustomer();
        Master m1 = createMaster();
        Customer c2 = createCustomer();
        Master m2 = createMaster();

        Visit v1 = createVisit(c1, m1, createInstatn(12), VisitStatus.SCHEDULED);
        createVisit(c1, m2, createInstatn(13), VisitStatus.CANCELLED);
        Visit v3 = createVisit(c2, m2, createInstatn(9), VisitStatus.FINISHED);

        VisitFilter filter = new VisitFilter();
        filter.setCustomerId(c1.getId());
        filter.setMasterId(m1.getId());
        filter.setStartAtFrom(createInstatn(12));
        filter.setStartAtTo(createInstatn(13));
        filter.setStatuses(Set.of(VisitStatus.SCHEDULED));
        Assertions.assertThat(visitService.getVisits(filter)).extracting("id")
                .containsExactlyInAnyOrder(v1.getId());
    }

    @Test
    public void testCreateVisit() {
        Customer c = createCustomer();
        Master m = createMaster();
        VisitCreateDTO create = new VisitCreateDTO();
        create.setStartAt(createInstatn(4));
        create.setFinishAt(createInstatn(5));
        create.setCustomerId(c.getId());
        create.setMasterId(m.getId());

        VisitReadDTO read = visitService.createVisit(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());
        Assert.assertEquals(VisitStatus.SCHEDULED, read.getStatus());

        Visit savedVisit = visitRepository.findById(read.getId()).get();
        Assertions.assertThat(savedVisit).isEqualToIgnoringGivenFields(read, "customer", "master", "customerMark");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateVisitWithWrongCustomer() {
        Master m = createMaster();
        VisitCreateDTO create = new VisitCreateDTO();
        create.setStartAt(createInstatn(4));
        create.setFinishAt(createInstatn(5));
        create.setCustomerId(UUID.randomUUID());
        create.setMasterId(m.getId());

        visitService.createVisit(create);
    }

    //***************************************************
    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("Joe");
        customer.setPhone("1234");
        return customerRepository.save(customer);
    }

    private Master createMaster() {
        Master master = new Master();
        master.setName("Qqq");
        master.setPhone("777");
        master.setAbout("The best master");
        return masterRepository.save(master);
    }

    private Visit createVisit(Customer customer, Master master) {
        Visit visit = new Visit();
        visit.setStartAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        visit.setStatus(VisitStatus.SCHEDULED);
        visit.setCustomer(customer);
        visit.setMaster(master);
        return visitRepository.save(visit);
    }

    private Visit createVisit(Customer customer, Master master, Instant startAt, VisitStatus visitStatus) {
        Visit visit = new Visit();
        visit.setCustomer(customer);
        visit.setMaster(master);
        visit.setStartAt(startAt);
        //visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        visit.setStatus(visitStatus);
        return visitRepository.save(visit);
    }

    private Instant createInstatn(int hour) {
        ZoneOffset utc = ZoneOffset.UTC;
        return LocalDateTime.of(2020, 01, 31, hour, 0, 0).toInstant(utc);
    }
}
