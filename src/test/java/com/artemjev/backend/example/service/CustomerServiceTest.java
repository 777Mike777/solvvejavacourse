package com.artemjev.backend.example.service;

import com.artemjev.backend.example.domain.Customer;
import com.artemjev.backend.example.dto.customer.CustomerCreateDTO;
import com.artemjev.backend.example.dto.customer.CustomerPatchDTO;
import com.artemjev.backend.example.dto.customer.CustomerPutDTO;
import com.artemjev.backend.example.dto.customer.CustomerReadDTO;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.repository.CustomerRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CustomerServiceTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    @Test
    public void testGetCustomer() {
        Customer customer = createCustomer();
        CustomerReadDTO readDTO = customerService.getCustomer(customer.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(customer);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCustomerWrongId() {
        customerService.getCustomer(UUID.randomUUID());
    }

    @Test
    public void testCreateCustomer() {
        CustomerCreateDTO create = new CustomerCreateDTO();
        create.setName("Joe");
        create.setPhone("123");
        CustomerReadDTO read = customerService.createCustomer(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Customer customer = customerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(customer);
    }

    @Test
    public void testPatchCustomer() {
        Customer customer = createCustomer();
        CustomerPatchDTO patch = new CustomerPatchDTO();
        patch.setName("Sam");
        patch.setPhone("4567");
        CustomerReadDTO read = customerService.patchCustomer(customer.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        customer = customerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(customer);
    }

    @Test
    public void testPatchCustomerEmptyPatch() {
        Customer customer = createCustomer();
        CustomerPatchDTO patch = new CustomerPatchDTO();
        CustomerReadDTO read = customerService.patchCustomer(customer.getId(), patch);

        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getPhone());

        Customer customerAfterUpdate = customerRepository.findById(read.getId()).get();

        Assert.assertNotNull(customerAfterUpdate.getName());
        Assert.assertNotNull(customerAfterUpdate.getPhone());

        Assertions.assertThat(customer).isEqualToComparingFieldByField(customerAfterUpdate);
    }

    @Test
    public void testUpdateCustomer() {
        Customer customer = createCustomer();
        CustomerPutDTO put = new CustomerPutDTO();
        put.setName("Sam");
        put.setPhone("4567");
        CustomerReadDTO read = customerService.updateCustomer(customer.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        customer = customerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(customer);
    }

    @Test
    public void testUpdateCustomerEmptyUpdate() {
        Customer customer = createCustomer();
        CustomerPutDTO put = new CustomerPutDTO();
        CustomerReadDTO read = customerService.updateCustomer(customer.getId(), put);

        Assert.assertNull(read.getName());
        Assert.assertNull(read.getPhone());

        Customer customerAfterUpdate = customerRepository.findById(read.getId()).get();

        Assert.assertNull(customerAfterUpdate.getName());
        Assert.assertNull(customerAfterUpdate.getPhone());
    }

    @Test
    public void testDeleteCustomer() {
        Customer customer = createCustomer();

        customerService.deleteCustomer(customer.getId());
        Assert.assertFalse(customerRepository.existsById(customer.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteCustomerNotFound() {
        customerService.deleteCustomer(UUID.randomUUID());
    }

    //**********************************************
    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("Joe");
        customer.setPhone("123");
        return customerRepository.save(customer);
    }
}
