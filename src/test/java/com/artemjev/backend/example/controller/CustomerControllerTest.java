package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.domain.Customer;
import com.artemjev.backend.example.dto.customer.CustomerCreateDTO;
import com.artemjev.backend.example.dto.customer.CustomerPatchDTO;
import com.artemjev.backend.example.dto.customer.CustomerPutDTO;
import com.artemjev.backend.example.dto.customer.CustomerReadDTO;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CustomerService customerService;

    @Test
    public void testGetCustomer() throws Exception {
        CustomerReadDTO customer = createCuctomerRead();

        Mockito.when(customerService.getCustomer(customer.getId())).thenReturn(customer);

        String resultJson = mvc.perform(get("/api/v1/customers/{id}", customer.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CustomerReadDTO actualCustomer = objectMapper.readValue(resultJson, CustomerReadDTO.class);
        Assertions.assertThat(actualCustomer).isEqualToComparingFieldByField(customer);

        Mockito.verify(customerService).getCustomer(customer.getId());
    }

    @Test
    public void testGetCustomerWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Customer.class, wrongId);
        Mockito.when(customerService.getCustomer(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/customers/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateCustomer() throws Exception {

        CustomerCreateDTO create = new CustomerCreateDTO();
        create.setName("Joe");
        create.setPhone("123");

        CustomerReadDTO read = createCuctomerRead();

        Mockito.when(customerService.createCustomer(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/customers")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CustomerReadDTO actualCustomer = objectMapper.readValue(resultJson, CustomerReadDTO.class);
        Assertions.assertThat(actualCustomer).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchCustomer() throws Exception {
        CustomerPatchDTO patchDTO = new CustomerPatchDTO();
        patchDTO.setName("Joe");
        patchDTO.setPhone("123");

        CustomerReadDTO read = createCuctomerRead();

        Mockito.when(customerService.patchCustomer(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/customers/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CustomerReadDTO actualCustomer = objectMapper.readValue(resultJson, CustomerReadDTO.class);
        Assert.assertEquals(read, actualCustomer);
    }

    @Test
    public void testPutCustomer() throws Exception {
        CustomerPutDTO putDTO = new CustomerPutDTO();
        putDTO.setName("Joe");
        putDTO.setPhone("123");

        CustomerReadDTO read = createCuctomerRead();

        Mockito.when(customerService.updateCustomer(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/customers/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CustomerReadDTO actualCustomer = objectMapper.readValue(resultJson, CustomerReadDTO.class);
        Assert.assertEquals(read, actualCustomer);
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/customers/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(customerService).deleteCustomer(id);
    }

    //*****************************************
    private CustomerReadDTO createCuctomerRead() {
        CustomerReadDTO read = new CustomerReadDTO();
        read.setId(UUID.randomUUID());
        read.setName("Joe");
        read.setPhone("123");
        return read;
    }
}
