package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.domain.Person;
import com.artemjev.backend.example.domain.Sex;
import com.artemjev.backend.example.dto.person.PersonCreateDTO;
import com.artemjev.backend.example.dto.person.PersonPatchDTO;
import com.artemjev.backend.example.dto.person.PersonPutDTO;
import com.artemjev.backend.example.dto.person.PersonReadDTO;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.exception.handler.ErrorInfo;
import com.artemjev.backend.example.service.PersonService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PersonService personService;

    @Test
    public void testGetPerson() throws Exception {
        PersonReadDTO person = createPersonRead();

        Mockito.when(personService.getPerson(person.getId())).thenReturn(person);

        String resultJson = mvc.perform(get("/api/v1/persons/{id}", person.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(person);
        Mockito.verify(personService).getPerson(person.getId());
    }

    @Test
    public void testGetPersonWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Person.class, wrongId);
        Mockito.when(personService.getPerson(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/persons/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetPersonWrongIdFormat() throws Exception {
        String resultJson = mvc.perform(get("/api/v1/persons/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
        try {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
            objectMapper.readValue(resultJson, ErrorInfo.class);
        } catch (JsonProcessingException e) {
            Assert.fail("JSON format of error is not correct");
        } finally {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
    }

    @Test
    public void testCreatePerson() throws Exception {
        PersonCreateDTO create = new PersonCreateDTO();
        create.setFirstName("Joe");
        create.setLastName("First");
        create.setPhone("123");
        create.setSex(Sex.UNDEFINED);
        create.setBirthDay(LocalDate.parse("2000-10-01"));
        create.setDeathDay(null);
        create.setMaritalStatus("bachelor");
        create.setChildrenAmount((byte) 0);
        create.setNationality("Reptiloid");
        create.setCitizenship("Yankee");
        create.setResidencePlace("Ukraine");
        create.setAutobiography("There is no data");
        create.setAdditionalInfo("There is no data");

        PersonReadDTO read = createPersonRead();

        Mockito.when(personService.createPerson(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/persons")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchPerson() throws Exception {
        PersonPatchDTO patch = new PersonPatchDTO();
        patch.setFirstName("Joe");
        patch.setLastName("First");
        patch.setPhone("123");
        patch.setSex(Sex.UNDEFINED);
        patch.setBirthDay(LocalDate.parse("2000-10-01"));
        patch.setDeathDay(null);
        patch.setMaritalStatus("bachelor");
        patch.setChildrenAmount((byte) 0);
        patch.setNationality("Reptiloid");
        patch.setCitizenship("Yankee");
        patch.setResidencePlace("Ukraine");
        patch.setAutobiography("There is no data");
        patch.setAdditionalInfo("There is no data");

        PersonReadDTO read = createPersonRead();

        Mockito.when(personService.patchPerson(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualPerson);
    }

    @Test
    public void testPutPerson() throws Exception {
        PersonPutDTO put = new PersonPutDTO();
        put.setFirstName("Joe");
        put.setLastName("First");
        put.setPhone("123");
        put.setSex(Sex.UNDEFINED);
        put.setBirthDay(LocalDate.parse("2000-10-01"));
        put.setDeathDay(null);
        put.setMaritalStatus("bachelor");
        put.setChildrenAmount((byte) 0);
        put.setNationality("Reptiloid");
        put.setCitizenship("Yankee");
        put.setResidencePlace("Ukraine");
        put.setAutobiography("There is no data");
        put.setAdditionalInfo("There is no data");

        PersonReadDTO read = createPersonRead();
        Mockito.when(personService.updatePerson(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualPerson);
    }

    @Test
    public void testDeletePerson() throws Exception {
        UUID id = UUID.randomUUID();
        mvc.perform(delete("/api/v1/persons/{id}", id.toString())).andExpect(status().isOk());
        Mockito.verify(personService).deletePerson(id);
    }

    //*******private-methods****************
    private PersonReadDTO createPersonRead() {

        PersonReadDTO read = new PersonReadDTO();
        read.setId(UUID.randomUUID());
        read.setFirstName("Joe");
        read.setLastName("First");
        read.setPhone("123");
        read.setSex(Sex.UNDEFINED);
        read.setBirthDay(LocalDate.parse("2000-10-01"));
        read.setDeathDay(null);
        read.setMaritalStatus("bachelor");
        read.setChildrenAmount((byte) 0);
        read.setNationality("Reptiloid");
        read.setCitizenship("Yankee");
        read.setResidencePlace("Ukraine");
        read.setAutobiography("There is no data");
        read.setAdditionalInfo("There is no data");
        return read;
    }
}
