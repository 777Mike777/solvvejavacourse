package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.domain.Customer;
import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.domain.Visit;
import com.artemjev.backend.example.domain.VisitStatus;
import com.artemjev.backend.example.dto.visit.VisitFilter;
import com.artemjev.backend.example.dto.visit.VisitReadDTO;
import com.artemjev.backend.example.service.VisitService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = VisitController.class)
public class VisitControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private VisitService visitService;

    @Test
    public void testGetVisits() throws Exception {
        VisitFilter visitFilter = new VisitFilter();
        visitFilter.setCustomerId(UUID.randomUUID());
        visitFilter.setMasterId(UUID.randomUUID());
        visitFilter.setStatuses(Set.of(VisitStatus.SCHEDULED, VisitStatus.FINISHED));
        visitFilter.setStartAtFrom(Instant.now().truncatedTo(ChronoUnit.MICROS));
        visitFilter.setStartAtTo(Instant.now().truncatedTo(ChronoUnit.MICROS));

        VisitReadDTO read = new VisitReadDTO();
        read.setCustomerId(visitFilter.getCustomerId());
        read.setMasterId(visitFilter.getMasterId());
        read.setStatus(VisitStatus.SCHEDULED);
        read.setId(UUID.randomUUID());
        read.setStartAt(Instant.now());
        read.setFinishAt(Instant.now());
        List<VisitReadDTO> expectedResult = List.of(read);
        Mockito.when(visitService.getVisits(visitFilter)).thenReturn(expectedResult);

        String resultJson = mvc.perform(get("/api/v1/visits")
                .param("customerId", visitFilter.getCustomerId().toString())
                .param("masterId", visitFilter.getMasterId().toString())
                .param("statuses", "SCHEDULED, FINISHED")
                .param("startAtFrom", visitFilter.getStartAtFrom().toString())
                .param("startAtTo", visitFilter.getStartAtTo().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<VisitReadDTO> actualResult = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testCreateVisit() throws Exception {
        Assert.assertTrue(true);
    }

    //***************************************************
    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("Joe");
        customer.setPhone("1234");
        return customer;
    }

    private Master createMaster() {
        Master master = new Master();
        master.setName("Qqq");
        master.setPhone("777");
        master.setAbout("The best master");
        return master;
    }

    private Visit createVisit(Customer customer, Master master) {
        Visit visit = new Visit();
        visit.setCustomer(customer);
        visit.setMaster(master);
        return visit;
    }

    private Visit createVisit(Customer customer, Master master, Instant startAt, VisitStatus visitStatus) {
        Visit visit = new Visit();
        visit.setCustomer(customer);
        visit.setMaster(master);
        visit.setStartAt(startAt);
        visit.setStatus(visitStatus);
        return visit;
    }

    private Instant createInstatn(int hour) {
        ZoneOffset utc = ZoneOffset.UTC;
        return LocalDateTime.of(2020, 01, 31, hour, 0, 0).toInstant(utc);
    }
}
