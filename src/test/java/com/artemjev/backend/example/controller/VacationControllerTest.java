package com.artemjev.backend.example.controller;

import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.domain.Vacation;
import com.artemjev.backend.example.dto.vacation.VacationCreateDTO;
import com.artemjev.backend.example.dto.vacation.VacationPatchDTO;
import com.artemjev.backend.example.dto.vacation.VacationPutDTO;
import com.artemjev.backend.example.dto.vacation.VacationReadDTO;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.service.VacationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = VacationController.class)
public class VacationControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private VacationService vacationService;

    private Random random = new Random();

    @Test
    public void testGetVacation() throws Exception {
        VacationReadDTO vacation = createVacationRead();

        Mockito.when(vacationService.getMasterVacation(vacation.getMasterId(), vacation.getId())).thenReturn(vacation);

        String resultJson = mvc.perform(get("/api/v1/masters/{masterId}/vacations/{id}",
                vacation.getMasterId(), vacation.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        VacationReadDTO actualVacation = objectMapper.readValue(resultJson, VacationReadDTO.class);
        Assertions.assertThat(actualVacation).isEqualToComparingFieldByField(vacation);
        Mockito.verify(vacationService).getMasterVacation(vacation.getMasterId(), vacation.getId());
    }

    @Test
    public void testGetVacationWrongVacationId() throws Exception {
        UUID masterId = UUID.randomUUID();
        UUID wrongVacationId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Vacation.class, wrongVacationId);
        Mockito.when(vacationService.getMasterVacation(masterId, wrongVacationId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/masters/{masterId}/vacations/{id}", masterId,
                wrongVacationId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetVacationWrongMasterId() throws Exception {
        UUID wrongMasterId = UUID.randomUUID();
        UUID vacationId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Master.class, vacationId);
        Mockito.when(vacationService.getMasterVacation(wrongMasterId, vacationId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/masters/{masterId}/vacations/{id}", wrongMasterId,
                vacationId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateVacation() throws Exception {
        VacationReadDTO read = createVacationRead();

        VacationCreateDTO create = new VacationCreateDTO();
        create.setStartAt(read.getStartAt());
        create.setEndAt(read.getEndAt());

        Mockito.when(vacationService.createVacation(read.getMasterId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/masters/{masterId}/vacations", read.getMasterId()
                .toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        VacationReadDTO actualVacation = objectMapper.readValue(resultJson, VacationReadDTO.class);
        Assertions.assertThat(actualVacation).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchVacation() throws Exception {
        VacationReadDTO read = createVacationRead();

        VacationPatchDTO patch = new VacationPatchDTO();
        patch.setMasterId(read.getMasterId());
        patch.setStartAt(read.getStartAt());
        patch.setEndAt(read.getEndAt());

        Mockito.when(vacationService.patchMasterVacation(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/masters/{masterId}/vacations/{id}",
                read.getMasterId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        VacationReadDTO actualVacation = objectMapper.readValue(resultJson, VacationReadDTO.class);
        Assert.assertEquals(read, actualVacation);
    }

    @Test
    public void testPutVacation() throws Exception {
        VacationReadDTO read = createVacationRead();

        VacationPutDTO put = new VacationPutDTO();
        put.setMasterId(read.getMasterId());
        put.setStartAt(read.getStartAt());
        put.setEndAt(read.getEndAt());

        Mockito.when(vacationService.updateMasterVacation(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/masters/{masterId}/vacations/{id}",
                read.getMasterId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        VacationReadDTO actualVacation = objectMapper.readValue(resultJson, VacationReadDTO.class);
        Assert.assertEquals(read, actualVacation);
    }

    @Test
    public void testDeleteVacation() throws Exception {
        UUID masterId = UUID.randomUUID();
        UUID vacationId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/masters/{masterId}/vacations/{id}", masterId.toString(),
                vacationId.toString())).andExpect(status().isOk());

        Mockito.verify(vacationService).deleteVacation(vacationId);
    }

    //*****************************************
    private VacationReadDTO createVacationRead() {
        VacationReadDTO read = new VacationReadDTO();
        read.setId(UUID.randomUUID());
        read.setMasterId(UUID.randomUUID());
        read.setStartAt(LocalDate.now().plusDays(random.nextInt(7)));
        read.setEndAt(read.getStartAt().plusDays(30));
        return read;
    }
}

