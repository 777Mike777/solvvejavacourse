package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.domain.Person;
import com.artemjev.backend.example.domain.Sex;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements =
        {"delete from actor",
                "delete from person"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testSave() {
        Person c = new Person();
        personRepository.save(c);
        assertNotNull(c.getId());
        assertTrue(personRepository.findById(c.getId()).isPresent());
    }

    @Test
    public void testCreatedDateIsSet() {
        Person person = createPerson();

        Instant createdAtBeforeReload = person.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        person = personRepository.findById(person.getId()).get();

        Instant createdAtAfterReload = person.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedDateIsSet() {
        Person person = createPerson();

        Instant updatedAtBeforeUpdate = person.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        person.setBirthDay(person.getBirthDay().plus(1, ChronoUnit.MONTHS));
        person = personRepository.save(person);

        Instant updatedAtAfterUpdate = person.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
    }

    //*******private-methods****************
    private Person createPerson() {
        Person person = new Person();
        person.setId(UUID.randomUUID());
        person.setFirstName("Joe");
        person.setLastName("First");
        person.setPhone("123");
        person.setSex(Sex.UNDEFINED);
        person.setBirthDay(LocalDate.parse("2000-10-01"));
        person.setDeathDay(null);
        person.setMaritalStatus("bachelor");
        person.setChildrenAmount((byte) 0);
        person.setNationality("Reptiloid");
        person.setCitizenship("Yankee");
        person.setResidencePlace("Ukraine");
        person.setAutobiography("Some kind of autobiography");
        person.setAdditionalInfo("Some additional information");
        return personRepository.save(person);
    }
}

