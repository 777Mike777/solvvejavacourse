package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.domain.*;
import com.artemjev.backend.example.exception.EntityNotFoundException;
import com.artemjev.backend.example.repository.complaint.ComplaintRepository;
import com.artemjev.backend.example.repository.complaintobject.ComplaintObjectRepository;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import org.assertj.core.api.Assertions;
import org.hibernate.proxy.HibernateProxy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {
        "delete from like",
        "delete from complaint_object",
        "delete from complaint",
        "delete from reg_user",
        "delete from role",
        "delete from actor",
        "delete from film_source_language",
        "delete from language",
        "delete from film",
        "delete from person",
        "delete from visit",
        "delete from vacation",
        "delete from customer",
        "delete from master"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class RepositoryHelperTest {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private ComplaintObjectRepository complaintObjectRepository;


    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MasterRepository masterRepository;

//    @Autowired
//    private VacationRepository vacationRepository;

    @Autowired
    private VisitRepository visitRepository;

    private ZoneOffset utc = ZoneOffset.UTC;

    @Test
    public void testGetReferenceIfExist() {
        Person refPerson = repositoryHelper.getReferenceIfExist(Person.class, createPerson().getId());
        assertNotNull(refPerson);
        assert refPerson instanceof HibernateProxy;
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReferenceIfExistWrongId() {
        Person person = repositoryHelper.getReferenceIfExist(Person.class, UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testValidateExistsWrongId() {
        repositoryHelper.validateExists(Person.class, UUID.randomUUID());
        assertTrue(false);
    }

    public void testValidateExists() {
        Person person = createPerson();
        repositoryHelper.validateExists(Person.class, person.getId());
        assertTrue(true);

    }

    @Test
    public void testGetPersonRequired() {
        Person person = createPerson();
        Person savedPerson = repositoryHelper.getEntityRequired(Person.class, person.getId());
        assertNotNull(savedPerson);
        Assertions.assertThat(person).isEqualToComparingFieldByField(savedPerson);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetPersonRequiredWrongId() {
        repositoryHelper.getEntityRequired(Person.class, UUID.randomUUID());
    }

    @Test
    public void testGetComplaintObjectRequired() {
        Complaint complaint = createComplaint();
        ComplaintObject complaintObject = createComplaintObject(complaint.getId());

        ComplaintObject savedComplaintObject = repositoryHelper.getEntityRequired(ComplaintObject.class, complaintObject.getId());
        assertNotNull(savedComplaintObject);
        Assertions.assertThat(complaintObject).isEqualToIgnoringGivenFields(savedComplaintObject, "complaint");
    }

    @Test
    public void testGetComplaintObjectRequiredWrongComplaintObjectId() {
        Complaint complaint = createComplaint();
        try {
            repositoryHelper.getEntityRequired(ComplaintObject.class, UUID.randomUUID());
        } catch (EntityNotFoundException ex) {
            Assert.assertTrue(ex.getMessage().contains(String.format("Entity %s with", ComplaintObject.class.getSimpleName())));
        }
    }

    //*******private-methods****************
    private Person createPerson() {
        Person person = new Person();
        person.setId(UUID.randomUUID());
        person.setFirstName("Joe");
        person.setLastName("First");
        person.setPhone("123");
        person.setSex(Sex.UNDEFINED);
        person.setBirthDay(LocalDate.parse("2000-10-01"));
        person.setDeathDay(null);
        person.setMaritalStatus("bachelor");
        person.setChildrenAmount((byte) 0);
        person.setNationality("Reptiloid");
        person.setCitizenship("Yankee");
        person.setResidencePlace("Ukraine");
        person.setAutobiography("Some kind of autobiography");
        person.setAdditionalInfo("Some additional information");
        return personRepository.save(person);
    }

    private RegUser createUser() {
        RegUser user = new RegUser();
        user.setId(UUID.randomUUID());
        user.setLogin("Login_1");
        user.setPassword("Password_1");
        user.setEmail("Login_1@email.com");
        user.setTrustLevel(7);
        return regUserRepository.save(user);
    }

    private Complaint createComplaint() {
        Complaint complaint = new Complaint();
        complaint.setId(UUID.randomUUID());
        complaint.setStatus(ComplaintStatus.UNTRIED);
        complaint.setTargetType(ComplaintTargType.CONTENT_MANAGER);
        return complaintRepository.save(complaint);
    }


    private ComplaintObject createComplaintObject(UUID complaintId) {
        Complaint complaint = Optional.ofNullable(complaintRepository.findById(complaintId).orElseThrow(() ->
                new EntityNotFoundException(Complaint.class, complaintId))).get();

        ComplaintObject complaintObject = new ComplaintObject();
        complaintObject.setId(UUID.randomUUID());
        complaintObject.setComplaint(complaint);
        complaintObject.setObjectType(DomainObjectType.REVIEW);
        complaintObject.setObjectId(UUID.randomUUID());
        complaintObject.setNote("Note");
        return complaintObjectRepository.save(complaintObject);
    }

//    private ComplaintObject createComplaintObject() {
//        Complaint complaint = createComplaint();
//
//        ComplaintObject complaintObject = new ComplaintObject();
//        complaintObject.setId(UUID.randomUUID());
//        complaintObject.setComplaint(complaint);
//        complaintObject.setObjectType(DomainObjectType.REVIEW);
//        complaintObject.setObjectId(UUID.randomUUID());
//        complaintObject.setNote("Note");
//        return complaintObjectRepository.save(complaintObject);
//    }

//    private ComplaintObject createComplaintObject(Complaint complaint) {
//        ComplaintObject complaintObject = new ComplaintObject();
//        complaintObject.setId(UUID.randomUUID());
//        complaintObject.setComplaint(complaint);
//        complaintObject.setObjectType(DomainObjectType.REVIEW);
//        complaintObject.setObjectId(UUID.randomUUID());
//        complaintObject.setNote("Note");
//        return complaintObjectRepository.save(complaintObject);
//    }

    //***************************************************
    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("Joe");
        customer.setPhone("1234");
        System.out.println("Customer was created " + customer);
        return customerRepository.save(customer);
    }

    private Master createMaster() {
        Master master = new Master();
        master.setName("Qqq");
        master.setPhone("777");
        master.setAbout("The best master");
        return masterRepository.save(master);
    }

    private Visit createVisit(Customer customer, Master master, VisitStatus visitStatus) {
        Visit visit = new Visit();
        visit.setStartAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        visit.setStatus(visitStatus);
        visit.setCustomer(customer);
        visit.setMaster(master);
        return visitRepository.save(visit);
    }

    private Visit createVisit(Customer customer, Master master, VisitStatus visitStatus, Instant startAt) {
        Visit visit = new Visit();
        visit.setStartAt(startAt);
        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        visit.setStatus(visitStatus);
        visit.setCustomer(customer);
        visit.setMaster(master);
        visit.setCustomerMark((int) (Math.random() * 100));
        return visitRepository.save(visit);
    }

    private Visit createVisit() {
        Customer c = createCustomer();
        Master m = createMaster();
        Visit visit = createVisit(c, m, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));
        return visitRepository.save(visit);
    }
}
