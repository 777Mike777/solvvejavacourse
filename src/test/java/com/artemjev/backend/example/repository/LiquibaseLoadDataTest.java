package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.repository.complaint.ComplaintRepository;
import com.artemjev.backend.example.repository.complaintobject.ComplaintObjectRepository;
import com.artemjev.backend.example.repository.vacation.VacationRepository;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
@Sql(statements = {
        "delete from misprint",
        "delete from domain_object",
        "delete from content_manager",
        "delete from like",
        "delete from complaint_object",
        "delete from complaint",
        "delete from reg_user",
        "delete from role",
        "delete from actor",
        "delete from film_source_language",
        "delete from language",
        "delete from film",
        "delete from person",
        "delete from visit",
        "delete from vacation",
        "delete from customer",
        "delete from master"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class LiquibaseLoadDataTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private VacationRepository vacationRepository;

    @Autowired
    private ComplaintRepository complaintRepository;

    @Autowired
    private ComplaintObjectRepository complaintObjectRepository;

    @Test
    public void testDataLoaded() {
        Assert.assertTrue(personRepository.count() > 0);
        Assert.assertTrue(complaintRepository.count() > 0);
        Assert.assertTrue(complaintObjectRepository.count() > 0);

        Assert.assertTrue(customerRepository.count() > 0);
        Assert.assertTrue(masterRepository.count() > 0);
        Assert.assertTrue(visitRepository.count() > 0);
        Assert.assertTrue(vacationRepository.count() > 0);
        // not finished
    }

}
