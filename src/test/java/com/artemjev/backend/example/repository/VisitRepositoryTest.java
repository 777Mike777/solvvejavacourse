package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.domain.Customer;
import com.artemjev.backend.example.domain.Master;
import com.artemjev.backend.example.domain.Visit;
import com.artemjev.backend.example.domain.VisitStatus;
import com.artemjev.backend.example.repository.visit.VisitRepository;
import com.artemjev.backend.example.service.TranslationService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {
        "delete from visit",
        "delete from customer",
        "delete from master"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class VisitRepositoryTest {

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private TranslationService translationService;

    private ZoneOffset utc = ZoneOffset.UTC;

    @Test
    public void testGetMasterVisits() {
        Customer c = createCustomer();
        Master m1 = createMaster();
        Master m2 = createMaster();

        Visit v1 = createVisit(c, m1, VisitStatus.SCHEDULED);
        createVisit(c, m1, VisitStatus.CANCELLED);

        Visit v2 = createVisit(c, m1, VisitStatus.SCHEDULED);
        createVisit(c, m2, VisitStatus.SCHEDULED);

        List<Visit> res = visitRepository.findByMasterIdAndStatusOrderByStartAtAsc(m1.getId(), VisitStatus.SCHEDULED);
        Assertions.assertThat(res).extracting(Visit::getId).isEqualTo(Arrays.asList(v1.getId(), v2.getId()));
    }

    @Test
    public void testGetMasterVisitsInInterval() {
        Customer c = createCustomer();
        Master m1 = createMaster();
        Master m2 = createMaster();

        Visit v1 = createVisit(c, m1, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));
        createVisit(c, m1, VisitStatus.CANCELLED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));

        Visit v2 = createVisit(c, m1, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 30, 0).toInstant(utc));
        createVisit(c, m1, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 17, 30, 0).toInstant(utc));
        createVisit(c, m2, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));

        List<Visit> res = visitRepository.findVisitsForMasterInGivenInterval(m1.getId(), VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc),
                LocalDateTime.of(2020, 01, 31, 17, 30, 0).toInstant(utc)
        );
        Assertions.assertThat(res).extracting(Visit::getId).isEqualTo(Arrays.asList(v1.getId(), v2.getId()));
        Assertions.assertThat(res).extracting(Visit::getId).containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetMasterVisitsInIntervalStrartToNull() {
        Customer c = createCustomer();
        Master m1 = createMaster();
        Master m2 = createMaster();

        Visit v1 = createVisit(c, m1, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));
        createVisit(c, m1, VisitStatus.CANCELLED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));

        Visit v2 = createVisit(c, m1, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 30, 0).toInstant(utc));
        Visit v3 = createVisit(c, m1, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 17, 30, 0).toInstant(utc));
        createVisit(c, m2, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));

        List<Visit> res = visitRepository.findVisitsForMasterInGivenInterval(m1.getId(), VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc),
                null);

        Assertions.assertThat(res).extracting(Visit::getId).containsExactlyInAnyOrder();
    }

    @Test
    public void testCreatedDateIsSet() {
        Visit v = createVisit();

        Instant createdAtBeforeReload = v.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        v = visitRepository.findById(v.getId()).get();

        Instant createdAtAfterReload = v.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedDateIsSet() {
        Visit v = createVisit();

        Instant updatedAtBeforeUpdate = v.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeUpdate);

        v.setStartAt(v.getStartAt().plus(10, ChronoUnit.MINUTES));
        v = visitRepository.save(v);

        Instant updatedAtAfterUpdate = v.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterUpdate);
        Assert.assertNotEquals(updatedAtBeforeUpdate, updatedAtAfterUpdate);
    }

    //***************************************************
    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setName("Joe");
        customer.setPhone("1234");
        System.out.println("Customer was created " + customer);
        return customerRepository.save(customer);
    }

    private Master createMaster() {
        Master master = new Master();
        master.setName("Qqq");
        master.setPhone("777");
        master.setAbout("The best master");
        return masterRepository.save(master);
    }

    private Visit createVisit(Customer customer, Master master, VisitStatus visitStatus) {
        Visit visit = new Visit();
        visit.setStartAt(Instant.now().truncatedTo(ChronoUnit.MICROS));
        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        visit.setStatus(visitStatus);
        visit.setCustomer(customer);
        visit.setMaster(master);
        return visitRepository.save(visit);
    }

    private Visit createVisit(Customer customer, Master master, VisitStatus visitStatus, Instant startAt) {
        Visit visit = new Visit();
        visit.setStartAt(startAt);
        visit.setFinishAt(visit.getStartAt().plus(1, ChronoUnit.HOURS));
        visit.setStatus(visitStatus);
        visit.setCustomer(customer);
        visit.setMaster(master);
        visit.setCustomerMark((int) (Math.random() * 100));
        return visitRepository.save(visit);
    }

    private Visit createVisit() {
        Customer c = createCustomer();
        Master m = createMaster();
        Visit visit = createVisit(c, m, VisitStatus.SCHEDULED,
                LocalDateTime.of(2020, 01, 31, 15, 0, 0).toInstant(utc));
        return visitRepository.save(visit);
    }
}