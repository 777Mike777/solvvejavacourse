package com.artemjev.backend.example.repository;

import com.artemjev.backend.example.domain.Master;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.support.TransactionTemplate;

import javax.transaction.Transaction;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;


@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = "delete from master", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class MasterRepositoryTest {

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetIdsOfMasters() {
        Set<UUID> expectedIdsOfMasters = new HashSet<>();
        expectedIdsOfMasters.add(createMaster().getId());
        expectedIdsOfMasters.add(createMaster().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfMasters, masterRepository.getIdsOfMasters().collect(Collectors.toSet()));
        });
    }

    //*******private-methods****************
    private Master createMaster() {
        Master master = new Master();
        master.setId(UUID.randomUUID());
        master.setName("Master-1");
        master.setPhone("123");
        master.setAbout("Something about");
//        master.setAverageMark(5.);
        return masterRepository.save(master);
    }
}
